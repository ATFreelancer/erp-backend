<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeBenefitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_benefits', function(Blueprint $table)
		{
			$table->foreign('employee_contract_id', 'fk_employee_benefits')->references('id')->on('employee_contracts')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('benefit_id', 'fk_employee_benefits_0')->references('id')->on('benefits')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_benefits', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_benefits');
			$table->dropForeign('fk_employee_benefits_0');
		});
	}

}
