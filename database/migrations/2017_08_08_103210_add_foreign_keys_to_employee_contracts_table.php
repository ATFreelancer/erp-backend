<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeContractsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_contracts', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'fk_employee_contracts')->references('id')->on('employees')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('contract_type_id', 'fk_employee_contracts_0')->references('id')->on('contract_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('position_id', 'fk_employee_contracts_1')->references('id')->on('positions')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_contracts', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_contracts');
			$table->dropForeign('fk_employee_contracts_0');
			$table->dropForeign('fk_employee_contracts_1');
		});
	}

}
