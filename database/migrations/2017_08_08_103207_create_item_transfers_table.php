<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_transfers', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('inventory_id')->nullable()->index('idx_item_transfer');
			$table->bigInteger('affiliation_sender_id')->nullable()->index('idx_item_transfer_0');
			$table->bigInteger('affiliation_receiver_id')->nullable()->index('idx_item_transfer_1');
			$table->timestamps();
			$table->decimal('quantity', 10, 0)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_transfers');
	}

}
