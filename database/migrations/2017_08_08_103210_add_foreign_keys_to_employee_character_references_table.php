<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeCharacterReferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_character_references', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'fk_employee_character_references')->references('id')->on('employees')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('company_id', 'fk_employee_character_references_0')->references('id')->on('companies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('position_id', 'fk_employee_character_references_1')->references('id')->on('positions')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_character_references', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_character_references');
			$table->dropForeign('fk_employee_character_references_0');
			$table->dropForeign('fk_employee_character_references_1');
		});
	}

}
