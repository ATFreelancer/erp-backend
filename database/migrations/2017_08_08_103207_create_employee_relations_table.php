<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeRelationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_relations', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_id')->index('idx_employee_parents');
			$table->string('first_name', 100);
			$table->string('middle_name', 100)->nullable();
			$table->string('last_name', 100);
			$table->string('suffix', 10)->nullable();
			$table->string('relation_type', 10)->comment('Mother
Father
Wife
Husband
Son
Daughter');
			$table->date('birthdate')->nullable();
			$table->bigInteger('city_id')->nullable()->index('idx_employee_relation_1');
			$table->string('address', 100)->nullable();
			$table->string('occupation', 100)->nullable();
			$table->bigInteger('province_id')->nullable()->index('idx_employee_relation_0');
			$table->bigInteger('country_id')->nullable()->index('idx_employee_relation');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_relations');
	}

}
