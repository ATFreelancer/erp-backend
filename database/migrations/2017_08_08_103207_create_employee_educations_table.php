<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeEducationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_educations', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_id')->index('idx_employee_educations');
			$table->string('elementary', 200)->nullable();
			$table->string('highschool', 200)->nullable();
			$table->string('college', 200)->nullable();
			$table->string('graduate_school', 200)->nullable();
			$table->string('elementary_grad_year', 4)->nullable();
			$table->string('highschool_grad_year', 4)->nullable();
			$table->string('college_grad_year', 4)->nullable();
			$table->string('graduate_school_grad_year', 4)->nullable();
			$table->text('degree', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_educations');
	}

}
