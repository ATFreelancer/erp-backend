<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_schedules', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			//$table->bigInteger('employee_id')->index('idx_employee_schedules_1');
			$table->bigInteger('contract_id', false);
			$table->bigInteger('schedule_id')->nullable()->index('idx_employee_schedules_0');
			$table->decimal('week_number', 10, 0)->nullable();
			$table->bigInteger('day_id')->index('idx_employee_schedules');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_schedules');
	}

}
