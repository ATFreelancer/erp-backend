<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeEmploymentRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_employment_records', function(Blueprint $table)
		{
			$table->foreign('position_id', 'fk_employee_employment_records')->references('id')->on('positions')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('employee_id', 'fk_employee_records')->references('id')->on('employees')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('company_id', 'fk_employee_records_0')->references('id')->on('companies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_employment_records', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_employment_records');
			$table->dropForeign('fk_employee_records');
			$table->dropForeign('fk_employee_records_0');
		});
	}

}
