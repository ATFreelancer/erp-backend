<?php

// use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutoffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutoffs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('frequency', 20)->nullable();
            $table->integer('pay_1', false,  2)->nullable();
            $table->integer('pay_2', false, 2)->nullable();
            $table->string('pay_4', 3)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cutoffs');
    }
}
