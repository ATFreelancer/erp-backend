<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedules', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('name', 100);
			$table->time('start_time');
			$table->time('end_time');
			$table->bigInteger('break_hour')->nullable();
			$table->bigInteger('break_minute')->nullable();
			$table->bigInteger('late_hour')->nullable();
			$table->bigInteger('late_minute')->nullable();
			$table->bigInteger('late_deduction_hour')->nullable();
			$table->bigInteger('late_deduction_minute')->nullable();
			$table->boolean('nobreak_sanction');
			$table->boolean('halfday_sanction');
			$table->boolean('absent_sanction');
			
			$table->text('description', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('schedules');
	}

}
