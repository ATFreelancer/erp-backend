<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeAdvancedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_advanceds', function(Blueprint $table)
		{
			/*$table->foreign('employee_id', 'fk_employee_advanceds')->references('id')->on('employees')->onUpdate('NO ACTION')->onDelete('NO ACTION');*/

			$table->foreign('employee_id', 'fk_employee_advanceds')
					->references('id')->on('employees')
					->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_advanceds', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_advanceds');
		});
	}

}
