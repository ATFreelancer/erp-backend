<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_schedules', function(Blueprint $table)
		{
			$table->foreign('day_id', 'fk_employee_schedules')->references('id')->on('days')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('schedule_id', 'fk_employee_schedules_0')->references('id')->on('schedules')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_schedules', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_schedules');
			$table->dropForeign('fk_employee_schedules_0');
		});
	}

}
