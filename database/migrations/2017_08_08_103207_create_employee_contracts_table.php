<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeContractsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_contracts', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_id')->index('idx_employee_contracts');
			$table->bigInteger('position_id')->index('idx_employee_contracts_1');
			$table->bigInteger('cutoff_id');
			$table->decimal('salary_per_month', 10, 2);
			$table->bigInteger('contract_type_id')->nullable()->index('idx_employee_contracts_0');
			$table->date('hire_date');
			$table->string('end_date', 10)->nullable();
			$table->date('regularization_duedate')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_contracts');
	}

}
