<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAttendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_attendances', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('employee_id');
            $table->string('date');
            $table->time('time_in')->nullable();
            $table->time('time_out')->nullable();
            $table->time('time_in_2')->nullable();
            $table->time('time_out_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
