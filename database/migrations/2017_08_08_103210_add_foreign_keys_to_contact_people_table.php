<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToContactPeopleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contact_people', function(Blueprint $table)
		{
			$table->foreign('company_id', 'fk_contact_people')->references('id')->on('companies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('city_id', 'fk_contact_people_0')->references('id')->on('cities')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('province_id', 'fk_contact_people_1')->references('id')->on('provinces')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('country_id', 'fk_contact_people_2')->references('id')->on('countries')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contact_people', function(Blueprint $table)
		{
			$table->dropForeign('fk_contact_people');
			$table->dropForeign('fk_contact_people_0');
			$table->dropForeign('fk_contact_people_1');
			$table->dropForeign('fk_contact_people_2');
		});
	}

}
