<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventories', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('item_id')->index('idx_inventory');
			$table->decimal('quantity', 10, 0)->nullable();
			$table->bigInteger('affiliation_id')->nullable()->index('idx_inventory_0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventories');
	}

}
