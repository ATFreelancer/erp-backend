<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeCharacterReferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_character_references', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_id')->nullable()->index('idx_employee_character_references');
			$table->string('first_name', 100)->nullable();
			$table->string('middle_name', 100)->nullable();
			$table->string('last_name', 100)->nullable();
			$table->string('suffix', 10)->nullable();
			$table->bigInteger('company_id')->nullable()->index('idx_employee_character_references_0');
			$table->bigInteger('position_id')->nullable()->index('idx_employee_character_references_1');
			$table->string('contact_number', 100)->nullable();
			$table->string('email_address', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_character_references');
	}

}
