<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeAdvancedDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_advanced_details', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_advanced_id')->index('idx_employee_advanced_details');
			/*$table->bigInteger('employee_advanced_id', false);*/
			$table->bigInteger('inventory_id')->nullable()->index('idx_employee_advanced_details_0');
			$table->decimal('quantity', 10, 0)->nullable();
			$table->decimal('cash', 10, 0)->nullable();
			$table->text('description', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_advanced_details');
	}

}
