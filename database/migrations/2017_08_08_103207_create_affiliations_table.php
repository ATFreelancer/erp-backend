<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAffiliationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('affiliations', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('name', 100)->nullable();
			$table->bigInteger('affiliation_type_id')->nullable()->index('idx_affiliation');
			$table->text('address', 65535)->nullable();
			$table->bigInteger('city_id')->nullable()->index('idx_affiliations');
			$table->bigInteger('province_id')->nullable()->index('idx_affiliations_0');
			$table->bigInteger('country_id')->nullable()->index('idx_affiliations_1');
			$table->string('contact_number', 100)->nullable();
			$table->string('email_address', 100)->nullable();
			$table->text('description', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('affiliations');
	}

}
