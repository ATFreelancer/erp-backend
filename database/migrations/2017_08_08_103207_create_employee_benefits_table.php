<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeBenefitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_benefits', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_contract_id')->index('idx_employee_benefits');
			$table->bigInteger('benefit_id')->index('idx_employee_benefits_0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_benefits');
	}

}
