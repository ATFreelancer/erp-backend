<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeLanguageSpokenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_language_spoken', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'fk_employee_language_spoken')->references('id')->on('employees')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('language_id', 'fk_employee_language_spoken_0')->references('id')->on('languages')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_language_spoken', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_language_spoken');
			$table->dropForeign('fk_employee_language_spoken_0');
		});
	}

}
