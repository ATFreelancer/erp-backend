<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->text('name', 65535);
			$table->text('address', 65535)->nullable();
			$table->bigInteger('city_id')->nullable()->index('idx_companies');
			$table->bigInteger('province_id')->nullable()->index('idx_companies_0');
			$table->bigInteger('country_id')->nullable()->index('idx_companies_1');
			$table->string('email_address', 100)->nullable();
			$table->string('contact_number', 100)->nullable();
			$table->text('description', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
