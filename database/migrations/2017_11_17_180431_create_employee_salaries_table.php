<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salaries', function ($table) {
            $table->increments('id');
            $table->integer('employee_id', false);
            $table->integer('contract_id', false);
            $table->string('period_start');
            $table->string('period_end');
            $table->decimal('hours', 10, 2);
            $table->decimal('basic', 10, 2)->default(0);
            $table->decimal('tardy', 10, 2)->default(0);
            $table->decimal('restdays', 10, 2)->default(0);
            $table->decimal('holidays', 10, 2)->default(0);
            $table->decimal('benefits', 10, 2)->default(0);
            $table->decimal('advances', 10, 2)->default(0);
            $table->decimal('deductions', 10, 2)->default(0);
            $table->decimal('total', 10, 2);
            $table->text('data')=>nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salaries');
    }
}
