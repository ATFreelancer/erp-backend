<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('username', 191);
            $table->string('password', 191);
            $table->string('first_name', 100);
            $table->string('middle_name', 100)->nullable();
            $table->string('last_name', 100);
            $table->string('gender', 10);
            $table->string('birthdate', 11);
            $table->string('address', 999);
            $table->string('contact_number', 100);
            $table->string('description', 999)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('remember_token', 100)->default('');
            $table->string('api_token', 60)->unique();
            $table->string('status', 11)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
