<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('first_name', 100);
			$table->string('middle_name', 100)->nullable();
			$table->string('last_name', 100);
			$table->string('suffix', 10)->nullable();
			$table->date('birthdate')->nullable();
			$table->bigInteger('nationality_id')->nullable()->index('idx_employees_2');
			$table->bigInteger('religion_id')->nullable()->index('idx_employees_3');
			$table->text('address', 65535)->nullable();
			$table->bigInteger('city_id')->nullable()->index('idx_employees_1');
			$table->bigInteger('province_id')->nullable()->index('idx_employees_0');
			$table->bigInteger('country_id')->nullable()->index('idx_employees');
			$table->string('contact_number', 100)->nullable();
			$table->string('email_address', 100)->nullable();
			$table->string('height', 10)->nullable();
			$table->string('weight', 10)->nullable();
			$table->dateTime('created_at')->nullable();
			$table->date('updated_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
