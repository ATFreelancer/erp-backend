<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeAdvancedDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_advanced_details', function(Blueprint $table)
		{
			/*$table->foreign('employee_advanced_id', 'fk_employee_advanced_details')->references('id')->on('employee_advanceds')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('inventory_id', 'fk_employee_advanced_details_0')->references('id')->on('inventories')->onUpdate('NO ACTION')->onDelete('NO ACTION');*/

			$table->foreign('employee_advanced_id', 'fk_employee_advanced_details')
					->references('id')->on('employee_advanceds')
					->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_advanced_details', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_advanced_details');
			$table->dropForeign('fk_employee_advanced_details_0');
		});
	}

}
