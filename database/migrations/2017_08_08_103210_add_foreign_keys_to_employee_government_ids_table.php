<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeGovernmentIdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_government_ids', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'fk_employee_government_ids')->references('id')->on('employees')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('government_id_type_id', 'fk_employee_government_ids_0')->references('id')->on('government_ids')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_government_ids', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_government_ids');
			$table->dropForeign('fk_employee_government_ids_0');
		});
	}

}
