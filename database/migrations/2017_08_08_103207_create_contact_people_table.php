<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactPeopleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_people', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('company_id')->index('idx_contact_people');
			$table->string('first_name', 100)->nullable();
			$table->string('middle_name', 100)->nullable();
			$table->string('last_name', 100)->nullable();
			$table->text('address', 65535)->nullable();
			$table->bigInteger('city_id')->nullable()->index('idx_contact_people_0');
			$table->bigInteger('province_id')->nullable()->index('idx_contact_people_1');
			$table->bigInteger('country_id')->nullable()->index('idx_contact_people_2');
			$table->string('email_address', 100)->nullable();
			$table->string('contact_number', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contact_people');
	}

}
