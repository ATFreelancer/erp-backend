<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeEmploymentRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_employment_records', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_id')->index('idx_employee_records');
			$table->bigInteger('company_id')->nullable()->index('idx_employee_records_0');
			$table->bigInteger('position_id')->nullable()->index('idx_employee_employment_records');
			$table->timestamps();
			$table->string('year_from', 4)->nullable();
			$table->string('year_to', 4)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_employment_records');
	}

}
