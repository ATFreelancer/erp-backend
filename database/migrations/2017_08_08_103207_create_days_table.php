<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('days', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->text('name', 65535);
			$table->string('abbreviation', 10)->nullable();
			$table->boolean('every_mon', 1)->nullable();
			$table->boolean('every_tue', 1)->nullable();
			$table->boolean('every_wed', 1)->nullable();
			$table->boolean('every_thu', 1)->nullable();
			$table->boolean('every_fri', 1)->nullable();
			$table->boolean('every_sat', 1)->nullable();
			$table->boolean('every_sun', 1)->nullable();
			$table->string('start_date',10)->nullable();
			$table->string('end_date',10)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('days');
	}

}
