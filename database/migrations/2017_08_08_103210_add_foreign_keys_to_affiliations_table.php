<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAffiliationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('affiliations', function(Blueprint $table)
		{
			$table->foreign('affiliation_type_id', 'fk_affiliation')->references('id')->on('affiliation_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('city_id', 'fk_affiliations')->references('id')->on('cities')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('province_id', 'fk_affiliations_0')->references('id')->on('provinces')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('country_id', 'fk_affiliations_1')->references('id')->on('countries')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('affiliations', function(Blueprint $table)
		{
			$table->dropForeign('fk_affiliation');
			$table->dropForeign('fk_affiliations');
			$table->dropForeign('fk_affiliations_0');
			$table->dropForeign('fk_affiliations_1');
		});
	}

}
