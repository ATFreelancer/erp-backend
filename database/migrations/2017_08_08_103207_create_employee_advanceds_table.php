<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeAdvancedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_advanceds', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('employee_id')->index('idx_employee_advanceds');
			$table->string('type', 20);
			$table->string('effect_date')->nullable();
			$table->string('pay_date')->nullable();
			$table->decimal('total', 10, 2)->nullable();
			$table->text('data')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_advanceds');
	}

}
