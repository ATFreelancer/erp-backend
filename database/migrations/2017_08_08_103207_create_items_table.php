<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->text('name', 65535);
			$table->bigInteger('company_id')->nullable()->index('idx_item');
			$table->bigInteger('unit_id')->nullable()->index('idx_item_0');
			$table->decimal('unit_value', 10, 2)->nullable();
			$table->bigInteger('packing_id')->nullable()->index('idx_item_1');
			$table->decimal('packing_value', 10, 2)->nullable();
			$table->decimal('unit_price', 10, 2)->nullable();
			$table->decimal('net_price', 10, 2)->nullable();
			$table->decimal('retail_price', 10, 2)->nullable();
			$table->decimal('wholesale_per_box', 10, 2)->nullable();
			$table->decimal('wholesale_per_piece', 10, 2)->nullable();
			$table->bigInteger('item_category_id')->nullable()->index('idx_item_2');
			$table->text('description', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
