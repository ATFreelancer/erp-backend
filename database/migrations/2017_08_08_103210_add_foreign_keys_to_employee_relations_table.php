<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmployeeRelationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('employee_relations', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'fk_employee_parents')->references('id')->on('employees')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('country_id', 'fk_employee_relation')->references('id')->on('countries')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('province_id', 'fk_employee_relation_0')->references('id')->on('provinces')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('city_id', 'fk_employee_relation_1')->references('id')->on('cities')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('employee_relations', function(Blueprint $table)
		{
			$table->dropForeign('fk_employee_parents');
			$table->dropForeign('fk_employee_relation');
			$table->dropForeign('fk_employee_relation_0');
			$table->dropForeign('fk_employee_relation_1');
		});
	}

}
