<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
</head>
<body>
	@php
		$fresh = $params[0];
		$low = $params[1];
		$transfer = $params[2];
		$noattendance = $params[3];
		$date = $params[4];
	@endphp
	<br>
	<center>
		<u>
			<h2 class="text-info">Daily Report - {{\Carbon\Carbon::parse($date)->format('F d, Y')}}</h2>
		</u>
	</center>
		<br>
		<br>
		<h3>Inventory</h3>
		<hr>
		<div class="row">
			
			<div class="col-md-4">
				<h3>Fresh Stocks</h3>
				<ol class="">
					@foreach($fresh as $f)
					<li><b>{{$f->name}}</b> ({{$f->unit_value}} {{$f->unit->abbreviation}})
						<span class="pull-right"> - {{$f->created_at->format('h:i a')}}</span>
					</li>
					@endforeach
				</ol>
				@if(count($fresh) == 0)
				<span>None</span>
				@endif
			</div>
			<div class="col-md-4">
				<h3>Transferred Stocks</h3>
				<ol class="">
					@foreach($transfer as $t)
					<li>{{$t->sender->name}} to {{$t->receiver->name}} <b>{{$t->inventory->item->name}}</b> ({{$t->inventory->item->unit_value}} {{$t->inventory->item->unit->abbreviation}}) x {{$t->quantity}}pcs
					<span class="pull-right"> - {{$t->created_at->format('h:i a')}}</span>
					</li>
					@endforeach
				</ol>
				@if(count($transfer) == 0)
				<span>None</span>
				@endif
			</div>


			<div class="col-md-4">
				<h3>Low in Stock</h3>
				<ul class="">
					@foreach($low as $l)
					<li>
						<b>{{$l->affiliation->name}}: {{$l->item->name}}</b> ({{$l->item->unit_value}} {{$l->item->unit->abbreviation}})
						<span>
							x {{$l->quantity}} left
						</span>
					</li>
					@endforeach
				</ul>
				@if(count($low) == 0)
				<span>None</span>
				@endif
			</div>
		</div>

		<h3>HR</h3>
		<hr>
		<div class="col-md-4">
			<h3>No Attendance (Employees with contracts)</h3>
			<ol class="">
				@foreach($noattendance as $n)
				<li><b>{{ucwords($n->first_name." ".$n->middle_name." ".$n->last_name)}}</b>
				</li>
				@endforeach
			</ol>
			@if(count($noattendance) == 0)
			<span>None</span>
			@endif
		</div>
</body>
</html>