<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\LoginController@login');
Route::post('/record', 'HR\EmployeeAttendanceController@record');
Route::get('/records', 'HR\EmployeeAttendanceController@records');

Route::get('/employees', 'HR\EmployeeController@getAll');
Route::get('/employees/{key}/{value}', 'HR\EmployeeController@getBy');
Route::get('/fingerregister', 'HR\EmployeeController@register');
Route::get('/recordfinger', 'HR\EmployeeController@recordFinger');
Route::get('/fingercheck', 'HR\EmployeeController@check');
Route::get('/checkfinger', 'HR\EmployeeController@checkFinger');
Route::get('/device.php', 'HR\EmployeeController@device');

Route::get('/employee_advanceds/{key}/{value}', 'HR\EmployeeAdvancedController@getBy');
Route::post('/employee_advanced', 'HR\EmployeeAdvancedController@post');

Route::post('/dailyreport', 'DailyReportController@mail');

Route::get('/inventories', 'Inventory\InventoryController@getAll');
Route::get('/pricelist/init', 'Inventory\BusinessLogic\BusinessLogicController@initPriceListConfig');
/*
* Route group with auth proctection
*/
Route::group(['middleware' => ['auth:api']], function () {
	/*
	 *Role Route
	 */
	Route::get('/roles', 'Account\RolesController@getAll');
	Route::get('/roles/{key}/{value}', 'Account\RolesController@getBy');
	Route::get('/role/{id}', 'Account\RolesController@get');
	Route::post('/role', 'Account\RolesController@post');
	Route::put('/role', 'Account\RolesController@put');
	Route::delete('/role', 'Account\RolesController@delete');

	/*
	 * Auth Route
	 */
	Route::get('/authuser', function(){
		return response()->json(['data' => Auth::User()], 201);
	});

	/*
	 * Cutoff Route
	 */
	Route::get('/cutoffs', 'HR\CutoffController@getAll');
	Route::get('/cutoffs/{key}/{value}', 'HR\CutoffController@getBy');
	Route::get('/cutoffs/{id}', 'HR\CutoffController@get');
	Route::post('/cutoff', 'HR\CutoffController@post');
	Route::put('/cutoff', 'HR\CutoffController@put');
	Route::delete('/cutoff', 'HR\CutoffController@delete');

	/*
	 * Employee Salary Route
	 */
	Route::get('/salaries', 'HR\EmployeeSalaryController@getAll');
	Route::get('/salaries/{key}/{value}', 'HR\EmployeeSalaryController@getBy');
	Route::get('/salaries/{id}', 'HR\EmployeeSalaryController@get');
	Route::post('/salary', 'HR\EmployeeSalaryController@post');
	Route::post('/generate', 'HR\EmployeeSalaryController@generate');
	Route::put('/salary', 'HR\EmployeeSalaryController@put');
	Route::delete('/salary', 'HR\EmployeeSalaryController@delete');


	/*
	 * Account Route
	 */
	Route::get('/accounts', 'Account\AccountController@getAll');
	Route::get('/accounts/{key}/{value}', 'Account\AccountController@getBy');
	Route::get('/account/{id}', 'Account\AccountController@get');
	Route::post('/account', 'Account\AccountController@post');
	Route::put('/account', 'Account\AccountController@put');
	Route::put('/changeaccount', 'Account\AccountController@change');
	Route::put('/reset', 'Account\AccountController@reset');
	Route::put('/disable', 'Account\AccountController@disable');
	Route::delete('/account', 'Account\AccountController@delete');

	/*
	 * Commons Module
	 */

	/*
	 * Affiliation Route
	 */

	Route::get('/affiliations', 'Commons\AffiliationController@getAll');
	Route::get('/affiliations/{key}/{value}', 'Commons\AffiliationController@getBy');
	Route::get('/affiliation/{id}', 'Commons\AffiliationController@get');
	Route::post('/affiliation', 'Commons\AffiliationController@post');
	Route::put('/affiliation', 'Commons\AffiliationController@put');
	Route::delete('/affiliation', 'Commons\AffiliationController@delete');

	/*
	 * Affiliation Type Route
	 */

	Route::get('/affiliation_types', 'Commons\AffiliationTypeController@getAll');
	Route::get('/affiliation_types/{key}/{value}', 'Commons\AffiliationTypeController@getBy');
	Route::get('/affiliation_type/{id}', 'Commons\AffiliationTypeController@get');
	Route::post('/affiliation_type', 'Commons\AffiliationTypeController@post');
	Route::put('/affiliation_type', 'Commons\AffiliationTypeController@put');
	Route::delete('/affiliation_type', 'Commons\AffiliationTypeController@delete');

	/*
	 * City Route
	 */

	Route::get('/cities', 'Commons\CityController@getAll');
	Route::get('/cities/{key}/{value}', 'Commons\CityController@getBy');
	Route::get('/city/{id}', 'Commons\CityController@get');
	Route::post('/city', 'Commons\CityController@post');
	Route::put('/city', 'Commons\CityController@put');
	Route::delete('/city', 'Commons\CityController@delete');

	/*
	 * Companies Route
	 */

	Route::get('/companies', 'Commons\CompanyController@getAll');
	Route::get('/companies/{key}/{value}', 'Commons\CompanyController@getBy');
	Route::get('/company/{id}', 'Commons\CompanyController@get');
	Route::post('/company', 'Commons\CompanyController@post');
	Route::put('/company', 'Commons\CompanyController@put');
	Route::delete('/company', 'Commons\CompanyController@delete');

	/*
	 * Contact Person Route
	 */

	Route::get('/contact_people', 'Commons\ContactPeopleController@getAll');
	Route::get('/contact_people/{key}/{value}', 'Commons\ContactPeopleController@getBy');
	Route::get('/contact_person/{id}', 'Commons\ContactPeopleController@get');
	Route::post('/contact_person', 'Commons\ContactPeopleController@post');
	Route::put('/contact_person', 'Commons\ContactPeopleController@put');
	Route::delete('/contact_person', 'Commons\ContactPeopleController@delete');
	Route::delete('/contact_person/all', 'Commons\ContactPeopleController@deleteAll');

	/*
	 * Country Route
	 */

	Route::get('/countries', 'Commons\CountryController@getAll');
	Route::get('/countries/{key}/{value}', 'Commons\CountryController@getBy');
	Route::get('/country/{id}', 'Commons\CountryController@get');
	Route::post('/country', 'Commons\CountryController@post');
	Route::put('/country', 'Commons\CountryController@put');
	Route::delete('/country', 'Commons\CountryController@delete');

	/*
	 * Day Route
	 */

	Route::get('/days', 'Commons\DayController@getAll');
	Route::get('/days/{key}/{value}', 'Commons\DayController@getBy');
	Route::get('/day/{id}', 'Commons\DayController@get');
	Route::post('/day', 'Commons\DayController@post');
	Route::put('/day', 'Commons\DayController@put');
	Route::delete('/day', 'Commons\DayController@delete');

	/*
	 * Government Id Route
	 */

	Route::get('/government_ids', 'Commons\GovernmentIdController@getAll');
	Route::get('/government_ids/{key}/{value}', 'Commons\GovernmentIdController@getBy');
	Route::get('/government_id/{id}', 'Commons\GovernmentIdController@get');
	Route::post('/government_id', 'Commons\GovernmentIdController@post');
	Route::put('/government_id', 'Commons\GovernmentIdController@put');
	Route::delete('/government_id', 'Commons\GovernmentIdController@delete');

	/*
	 * Language Route
	 */

	Route::get('/languages', 'Commons\LanguageController@getAll');
	Route::get('/languages/{key}/{value}', 'Commons\LanguageController@getBy');
	Route::get('/language/{id}', 'Commons\LanguageController@get');
	Route::post('/language', 'Commons\LanguageController@post');
	Route::put('/language', 'Commons\LanguageController@put');
	Route::delete('/language', 'Commons\LanguageController@delete');

	/*
	 * Packing Route
	 */

	Route::get('/packings', 'Commons\PackingController@getAll');
	Route::get('/packings/{key}/{value}', 'Commons\PackingController@getBy');
	Route::get('/packing/{id}', 'Commons\PackingController@get');
	Route::post('/packing', 'Commons\PackingController@post');
	Route::put('/packing', 'Commons\PackingController@put');
	Route::delete('/packing', 'Commons\PackingController@delete');

	/*
	 * Position Route
	 */

	Route::get('/positions', 'Commons\PositionController@getAll');
	Route::get('/positions/{key}/{value}', 'Commons\PositionController@getBy');
	Route::get('/position/{id}', 'Commons\PositionController@get');
	Route::post('/position', 'Commons\PositionController@post');
	Route::put('/position', 'Commons\PositionController@put');
	Route::delete('/position', 'Commons\PositionController@delete');

	/*
	 * Province Route
	 */

	Route::get('/provinces', 'Commons\ProvinceController@getAll');
	Route::get('/provinces/{key}/{value}', 'Commons\ProvinceController@getBy');
	Route::get('/province/{id}', 'Commons\ProvinceController@get');
	Route::post('/province', 'Commons\ProvinceController@post');
	Route::put('/province', 'Commons\ProvinceController@put');
	Route::delete('/province', 'Commons\ProvinceController@delete');

	/*
	 * Religion Route
	 */

	Route::get('/religions', 'Commons\ReligionController@getAll');
	Route::get('/religions/{key}/{value}', 'Commons\ReligionController@getBy');
	Route::get('/religion/{id}', 'Commons\ReligionController@get');
	Route::post('/religion', 'Commons\ReligionController@post');
	Route::put('/religion', 'Commons\ReligionController@put');
	Route::delete('/religion', 'Commons\ReligionController@delete');

	/*
	 * Unit Route
	 */

	Route::get('/units', 'Commons\UnitController@getAll');
	Route::get('/units/{key}/{value}', 'Commons\UnitController@getBy');
	Route::get('/unit/{id}', 'Commons\UnitController@get');
	Route::post('/unit', 'Commons\UnitController@post');
	Route::put('/unit', 'Commons\UnitController@put');
	Route::delete('/unit', 'Commons\UnitController@delete');


	/*
	 * HR Module
	 */

	/*
	 * Benefit Route
	 */

	Route::get('/benefits', 'HR\BenefitController@getAll');
	Route::get('/benefits/{key}/{value}', 'HR\BenefitController@getBy');
	Route::get('/benefit/{id}', 'HR\BenefitController@get');
	Route::post('/benefit', 'HR\BenefitController@post');
	Route::put('/benefit', 'HR\BenefitController@put');
	Route::delete('/benefit', 'HR\BenefitController@delete');

	/*
	 * Contract Type Route
	 */

	Route::get('/contract_types', 'HR\ContractTypeController@getAll');
	Route::get('/contract_types/{key}/{value}', 'HR\ContractTypeController@getBy');
	Route::get('/contract_type/{id}', 'HR\ContractTypeController@get');
	Route::post('/contract_type', 'HR\ContractTypeController@post');
	Route::put('/contract_type', 'HR\ContractTypeController@put');
	Route::delete('/contract_type', 'HR\ContractTypeController@delete');

	/*
	 * Employee Route
	 */

	Route::get('/employee/{id}', 'HR\EmployeeController@get');
	Route::post('/employee', 'HR\EmployeeController@post');
	Route::put('/employee', 'HR\EmployeeController@put');
	Route::put('/fingernote', 'HR\EmployeeController@fingernote');
	Route::delete('/employee', 'HR\EmployeeController@delete');

	/*
	 * Employee Advanced Route
	 */

	Route::get('/employee_advanceds', 'HR\EmployeeAdvancedController@getAll');
	Route::get('/employee_advanced/{id}', 'HR\EmployeeAdvancedController@get');
	Route::put('/employee_advanced', 'HR\EmployeeAdvancedController@put');
	Route::delete('/employee_advanced', 'HR\EmployeeAdvancedController@delete');

	/*
	 * Employee Advanced Details Route
	 */

	Route::get('/employee_advanced_details', 'HR\EmployeeAdvancedDetailController@getAll');
	Route::get('/employee_advanced_details/{key}/{value}', 'HR\EmployeeAdvancedDetailController@getBy');
	Route::get('/employee_advanced_detail/{id}', 'HR\EmployeeAdvancedDetailController@get');
	Route::post('/employee_advanced_detail', 'HR\EmployeeAdvancedDetailController@post');
	Route::put('/employee_advanced_detail', 'HR\EmployeeAdvancedDetailController@put');
	Route::delete('/employee_advanced_detail', 'HR\EmployeeAdvancedDetailController@delete');

	/*
	 * Employee Attendance Route
	 */

	Route::get('/employee_attendances', 'HR\EmployeeAttendanceController@getAll');
	

	Route::get('/employee_attendances/{key}/{value}', 'HR\EmployeeAttendanceController@getBy');
	Route::get('/employee_attendance/{id}', 'HR\EmployeeAttendanceController@get');
	Route::post('/employee_attendance', 'HR\EmployeeAttendanceController@post');
	Route::put('/employee_attendance', 'HR\EmployeeAttendanceController@put');
	Route::delete('/employee_attendance', 'HR\EmployeeAttendanceController@delete');

	/*
	 * Employee Benefit Route
	 */

	Route::get('/employee_benefits', 'HR\EmployeeBenefitController@getAll');
	Route::get('/employee_benefits/{key}/{value}', 'HR\EmployeeBenefitController@getBy');
	Route::get('/employee_benefit/{id}', 'HR\EmployeeBenefitController@get');
	Route::post('/employee_benefit', 'HR\EmployeeBenefitController@post');
	Route::put('/employee_benefit', 'HR\EmployeeBenefitController@put');
	Route::delete('/employee_benefit', 'HR\EmployeeBenefitController@delete');

	/*
	 * Employee Character Reference Route
	 */

	Route::get('/employee_character_references', 'HR\EmployeeCharacterReferenceController@getAll');
	Route::get('/employee_character_references/{key}/{value}', 'HR\EmployeeCharacterReferenceController@getBy');
	Route::get('/employee_character_reference/{id}', 'HR\EmployeeCharacterReferenceController@get');
	Route::post('/employee_character_reference', 'HR\EmployeeCharacterReferenceController@post');
	Route::put('/employee_character_reference', 'HR\EmployeeCharacterReferenceController@put');
	Route::delete('/employee_character_reference', 'HR\EmployeeCharacterReferenceController@delete');

	/*
	 * Employee Contracts Route
	 */

	Route::get('/employee_contracts', 'HR\EmployeeContractController@getAll');
	Route::get('/employee_contracts/{key}/{value}', 'HR\EmployeeContractController@getBy');
	Route::get('/employee_contract/{id}', 'HR\EmployeeContractController@get');
	Route::post('/employee_contract', 'HR\EmployeeContractController@post');
	Route::put('/employee_contract', 'HR\EmployeeContractController@put');
	Route::delete('/employee_contract', 'HR\EmployeeContractController@delete');

	/*
	 * Employee Education Route
	 */

	Route::get('/employee_educations', 'HR\EmployeeEducationController@getAll');
	Route::get('/employee_educations/{key}/{value}', 'HR\EmployeeEducationController@getBy');
	Route::get('/employee_education/{id}', 'HR\EmployeeEducationController@get');
	Route::post('/employee_education', 'HR\EmployeeEducationController@post');
	Route::put('/employee_education', 'HR\EmployeeEducationController@put');
	Route::delete('/employee_education', 'HR\EmployeeEducationController@delete');

	/*
	 * Employee Employment Record Route
	 */

	Route::get('/employee_employment_records', 'HR\EmployeeEmploymentRecordController@getAll');
	Route::get('/employee_employment_records/{key}/{value}', 'HR\EmployeeEmploymentRecordController@getBy');
	Route::get('/employee_employment_record/{id}', 'HR\EmployeeEmploymentRecordController@get');
	Route::post('/employee_employment_record', 'HR\EmployeeEmploymentRecordController@post');
	Route::put('/employee_employment_record', 'HR\EmployeeEmploymentRecordController@put');
	Route::delete('/employee_employment_record', 'HR\EmployeeEmploymentRecordController@delete');

	/*
	 * Employee Government Id Record Route
	 */

	Route::get('/employee_government_ids', 'HR\EmployeeGovernmentIdController@getAll');
	Route::get('/employee_government_ids/{key}/{value}', 'HR\EmployeeGovernmentIdController@getBy');
	Route::get('/employee_government_id/{id}', 'HR\EmployeeGovernmentIdController@get');
	Route::post('/employee_government_id', 'HR\EmployeeGovernmentIdController@post');
	Route::put('/employee_government_id', 'HR\EmployeeGovernmentIdController@put');
	Route::delete('/employee_government_id', 'HR\EmployeeGovernmentIdController@delete');

	/*
	 * Employee Language Spoken Route
	 */

	Route::get('/employee_language_spokens', 'HR\EmployeeLanguageSpokenController@getAll');
	Route::get('/employee_language_spokens/{key}/{value}', 'HR\EmployeeLanguageSpokenController@getBy');
	Route::get('/employee_language_spoken/{id}', 'HR\EmployeeLanguageSpokenController@get');
	Route::post('/employee_language_spoken', 'HR\EmployeeLanguageSpokenController@post');
	Route::put('/employee_language_spoken', 'HR\EmployeeLanguageSpokenController@put');
	Route::delete('/employee_language_spoken', 'HR\EmployeeLanguageSpokenController@delete');

	/*
	 * Employee Relation Route
	 */

	Route::get('/employee_relations', 'HR\EmployeeRelationController@getAll');
	Route::get('/employee_relations/{key}/{value}', 'HR\EmployeeRelationController@getBy');
	Route::get('/employee_relation/{id}', 'HR\EmployeeRelationController@get');
	Route::post('/employee_relation', 'HR\EmployeeRelationController@post');
	Route::put('/employee_relation', 'HR\EmployeeRelationController@put');
	Route::delete('/employee_relation', 'HR\EmployeeRelationController@delete');

	/*
	 * Employee Schedule Route
	 */

	Route::get('/employee_schedules', 'HR\EmployeeScheduleController@getAll');
	Route::get('/employee_schedules/{key}/{value}', 'HR\EmployeeScheduleController@getBy');
	Route::get('/employee_schedule/{id}', 'HR\EmployeeScheduleController@get');
	Route::post('/employee_schedule', 'HR\EmployeeScheduleController@post');
	Route::put('/employee_schedule', 'HR\EmployeeScheduleController@put');
	Route::delete('/employee_schedule', 'HR\EmployeeScheduleController@delete');

	/*
	 * Schedule Route
	 */

	Route::get('/schedules', 'HR\ScheduleController@getAll');
	Route::get('/schedules/{key}/{value}', 'HR\ScheduleController@getBy');
	Route::get('/schedule/{id}', 'HR\ScheduleController@get');
	Route::post('/schedule', 'HR\ScheduleController@post');
	Route::put('/schedule', 'HR\ScheduleController@put');
	Route::delete('/schedule', 'HR\ScheduleController@delete');


	/*
	 * Inventory Module
	 */

	/*
	 * Item Route
	 */

	Route::get('/items', 'Inventory\ItemController@getAll');
	Route::get('/items/{key}/{value}', 'Inventory\ItemController@getBy');
	Route::get('/item/{id}', 'Inventory\ItemController@get');
	Route::post('/item', 'Inventory\ItemController@post');
	Route::put('/item', 'Inventory\ItemController@put');
	Route::delete('/item', 'Inventory\ItemController@delete');

	/*
	 * Item Category Route
	 */

	Route::get('/item_categories', 'Inventory\ItemCategoryController@getAll');
	Route::get('/item_categories/{key}/{value}', 'Inventory\ItemCategoryController@getBy');
	Route::get('/item_category/{id}', 'Inventory\ItemCategoryController@get');
	Route::post('/item_category', 'Inventory\ItemCategoryController@post');
	Route::put('/item_category', 'Inventory\ItemCategoryController@put');
	Route::delete('/item_category', 'Inventory\ItemCategoryController@delete');

	/*
	 * Inventory Route
	 */
	Route::get('/inventories/{key}/{value}', 'Inventory\InventoryController@getBy');
	Route::get('/inventory/{id}', 'Inventory\InventoryController@get');
	Route::post('/inventory', 'Inventory\InventoryController@post');
	Route::put('/inventory', 'Inventory\InventoryController@put');
	Route::put('/saveAll', 'Inventory\InventoryController@saveAll');
	Route::delete('/inventory', 'Inventory\InventoryController@delete');

	/*
	 * Item Transfer Route
	 */

	Route::get('/item_transfers', 'Inventory\ItemTransferController@getAll');
	Route::get('/item_transfers/{key}/{value}', 'Inventory\ItemTransferController@getBy');
	Route::get('/item_transfer/{id}', 'Inventory\ItemTransferController@get');
	Route::post('/item_transfer', 'Inventory\ItemTransferController@post');
	Route::put('/item_transfer', 'Inventory\ItemTransferController@put');
	Route::delete('/item_transfer', 'Inventory\ItemTransferController@delete');

    /*
     * Commons Business Logic Route
     */

    Route::get('/commons/init', 'Commons\BusinessLogic\BusinessLogicController@init');

	/*
	 * Inventory Business Logic Route
	 */

	Route::get('/items/init', 'Inventory\BusinessLogic\BusinessLogicController@initItemsConfig');
	Route::post('/item_transfer/transact', 'Inventory\BusinessLogic\BusinessLogicController@transact');

	/*
	 * HR Business Logic Route
	 */

    Route::get('/employeelist/init', 'HR\BusinessLogic\BusinessLogicController@initEmployeeList');
    Route::get('/employeelist/retrieve/{id}', 'HR\BusinessLogic\BusinessLogicController@retrieve');
    Route::post('/employeelist/record', 'HR\BusinessLogic\BusinessLogicController@record');    
    Route::delete('/employeelist/clean', 'HR\BusinessLogic\BusinessLogicController@clean');

	Route::post('/employee_contract/record', 'HR\BusinessLogic\BusinessLogicController@recordContracts');
	Route::get('/employee_contract/retrieve/{id}', 'HR\BusinessLogic\BusinessLogicController@retrieveContracts');
	Route::delete('/employee_contract/clean', 'HR\BusinessLogic\BusinessLogicController@cleanContracts');
	
});


/*
 * Registration Route
 */
/*Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
*/
/*
 * Password Reset Route
 */
/*$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');*/
