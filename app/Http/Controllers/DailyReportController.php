<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Inventory\Inventory;
use App\Models\Inventory\ItemTransfer;
use App\Models\Inventory\Item;
use App\Models\HR\Employee;
use App\Models\HR\TimeRecord;
use App\MailSetting;

class DailyReportController extends Controller
{
    public function mail()
    {
    	set_time_limit(0);
    	$settings = MailSetting::where('name', 'Daily Report')->first();
    	$today = \Carbon\Carbon::now();
    	if (empty($settings->latest_date)) {
    		$settings['latest_date'] = date('Y-m-d');
    	}
    	if (empty($settings->time)) {
    		$settings['time'] = "20:00:00";
    	}
    	$latest = \Carbon\Carbon::parse($settings->latest_date ." ". $settings->time)->addDay();
    	$x = 0;
    	while($latest->format('Y-m-d H:i') < $today->copy()->format('Y-m-d H:i')){
    		$x++;
    		$maildate = $latest->copy()->format('Y-m-d');
    		$emails = User::pluck('email')->toArray();
	    	$fresh = Item::whereDate('created_at', $maildate)->with(['unit', 'packing'])->orderBy('created_at', 'desc')->get();
	    	$low = Inventory::where('quantity','<',10)->with(['item.unit', 'affiliation'])->orderBy('quantity')->get();
	    	$transfer = ItemTransfer::whereDate('created_at', $maildate)->with(['sender','receiver','inventory.item.unit'])->orderBy('created_at', 'desc')->get();

	    	$noattendance = TimeRecord::whereDate('created_at', $maildate)->pluck('employee_id')->toArray();
	    	$noattendance = Employee::whereNotIn('id', $noattendance)->withCount('contract')->with(['contract'])->orderBy('last_name')->get();
	    	$noattendance = $noattendance->where('contract_count', 1);		

			\Mail::to($emails)->send(new \App\Mail\DailyReport([$fresh, $low, $transfer, $noattendance, $latest->format('Y-m-d')]));

    		$latest->addDay();
    		if ($latest->copy()->format('Y-m-d') > date('Y-m-d')) {
    			break;
    		}
    	}
    	$settings->latest_date = date('Y-m-d');
    	$settings->save();
    }
}
