<?php

namespace App\Http\Controllers\Commons;

use App\Models\Commons\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;

class CompanyController extends BasedController
{
    private $api = Company::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value) {
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {        
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        
        return $this->_post($request, $this->api);
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) {        
        return $this->_delete($request, $this->api);
    }
}
