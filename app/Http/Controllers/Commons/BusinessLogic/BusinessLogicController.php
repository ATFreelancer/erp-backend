<?php
namespace App\Http\Controllers\Commons\BusinessLogic;

use DB;
use App\Models\Commons\Affiliation;
use App\Models\Commons\AffiliationType;
use App\Models\Commons\City;
use App\Models\Commons\Day;
use App\Models\Commons\GovernmentId;
use App\Models\Commons\Language;
use App\Models\Commons\Packing;
use App\Models\Commons\Province;
use App\Models\Commons\Unit;
use App\Models\Commons\Company;
use App\Models\Commons\Country;
use App\Models\Commons\Position;
use App\Models\Commons\Religion;
use App\Models\Inventory\ItemCategory;
use App\Models\HR\ContractType;
use App\Models\HR\Benefit;
use App\Models\HR\Schedule;
use App\User;
use App\Http\Controllers\BasedController;
use Illuminate\Http\Request;

class BusinessLogicController extends BasedController
{
    public function init() {
        $collections = (object)[];
        $collections->affiliations = Affiliation::all();
        $collections->affiliationTypes = AffiliationType::all();
        $collections->cities = City::all();
        $collections->companies = Company::all();
        $collections->countries = Country::all();
        $collections->days = Day::all();
        $collections->governmentIds = GovernmentId::all();
        $collections->languages = Language::all();
        $collections->packings = Packing::all();
        $collections->positions = Position::all();
        $collections->provinces = Province::all();
        $collections->religions = Religion::all();
        $collections->units = Unit::all();
        $collections->itemCategories = ItemCategory::all();
        $collections->contractTypes = ContractType::all();
        $collections->benefits = Benefit::all();
        $collections->schedules = Schedule::all();
        $collections->auth = \Auth::User();
        $collections->accounts = User::all();

        return response()->json($collections,200);
    }

}