<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    public function authenticated()
    {
        return response()->json(['data' => Auth::user()], 200);
    }

    public function username()
    {
        return 'username';
    }

    public function sendFailedLoginResponse()
    {
        return response()->json(['data' => ['auth' => false]],401);
    }

    public function authenticatedUser()
    {
        if (Auth::user()) {
            return response()->json(['data' => Auth::user()], 200);
        }
        return response()->json(['data' => ['auth' => false]]);
    }
}
