<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class BasedController extends Controller
{
    
    public function _getAll(Request $request, $api) {
        $request = request();
        $query = app($api)->newQuery();             
        
        if ($request->exists('include')) {

            foreach ($request->include as $key => $value) {
                $query = $query->with($value);
            }
        }

        if ($request->exists('joinBy')) {            
            foreach ($request->joinBy as $key => $value) {                   
                $joinBy = json_decode($value);            
                $query = $query->leftJoin($joinBy->join, $joinBy->attr1, $joinBy->attr2);          
            }     
        }

        if ($request->exists('filter')) {
            $res = json_decode($request->filter);            
            foreach ($res->condition as $con => $val) {
                $command = !empty($val->command) ? $val->command : 'where';
                if (!empty($val->operator)) {
                    $query->{$command}($val->where, $val->operator, $val->val);
                }else if(!empty($val->where)){
                    $query->{$command}($val->where, $val->val);
                }else {
                    $query->{$command}($val->val);
                }
            }
        }

        if ($request->exists('search')) {
            $search = json_decode($request->search);            
            if ($search->keyword) {                
                $value = "%{$search->keyword}%";
                $query = $query->where($search->by,'like', $value);    
            }
        }

        if ($request->exists('select')) {            
            foreach ($request->select as $key => $value) {                
                $query = $query->addSelect($value);
            }
            
        }
        
        if ($request->has('orderBy')) {            
            $orderBy = json_decode($request->orderBy);
            foreach ($orderBy->condition as $key => $value) {                
                if ($value->col) {
                    $query = $query->orderBy($value->col, $value->direction);    
                }                
            }
        } else {
            $query = $query->orderBy('id', 'asc');
        }        

        if ($request->has('per_page')) {            
            $perPage = $request->per_page === "-1" ? null : (int) $request->per_page;
        } else {
            $perPage = null;
        }

        // $perPage = request()->has('per_page') ? (int) request()->per_page : null;
        $pagination = $query->paginate($perPage);
        $pagination->appends([
            'include' => request()->include,
            'joinBy' => request()->joinBy,
            'orderBy' => request()->orderBy,
            'filter' => request()->filter,
            'per_page' => request()->per_page
        ]);
        
        /*$res = json_decode(@$request->filter);            
        if ($request->exists('filter') && @count($res->afterGet)>0) {
            $query = $pagination;
            foreach ($res->afterGet as $con => $val) {
                $command = !empty($val->command) ? $val->command : 'where';
                if (!empty($val->operator)) {
                    $pagination = $query->{$command}($val->where, $val->operator, $val->val);
                }else if(!empty($val->where)){
                    $pagination = $query->{$command}($val->where, $val->val);
                }else {
                    $pagination = $query->{$command}($val->val);
                }
            }
        }*/

        return response()->json($pagination,200);
    }

    public function _getBy($key, $value, $api)
    {
        $params = (array) $_GET;
        $voidGet = @key(@json_decode(@$params[0])) == 'voidGet';
        if ($voidGet) {
            unset($params[0]);
        }
        $datas = app($api)->newQuery();
        if (($key !='null' && $value != 'null')) {
            $val = is_object($value) ? $value->val : $value;
            $datas = $datas->where($key, $val);
        }
        $datas = $this->queryExtractor($params, $datas);        
        if (!$voidGet) {
            $datas = $datas->get();
        }
        return response()->json(['data'=>$datas],200);
    }

    public function queryExtractor($params, $datas)
    {
        foreach ($params as $keys => $scope) {
            $scope = (array) json_decode($scope);  
            $_key = key($scope);
            if (is_object(@$scope[$_key][0])) {
                $this->querySubParts($scope, $_key, $datas);
            }else {
                $datas = $this->queryParams($scope, $_key, $datas);
            }
        }

        return $datas;
    }

    public function queryParams($scope, $_key, $datas)
    {
        if (count($scope[$_key]) <= 1) {
            $datas = $datas->{$_key}(@$scope[$_key][0]);
        }else if (count($scope[$_key]) === 2){
            $datas = $datas->{$_key}(@$scope[$_key][0], @$scope[$_key][1]);
        }else if (count($scope[$_key]) === 3){
            $datas = $datas->{$_key}(@$scope[$_key][0], @$scope[$_key][1], @$scope[$_key][2]);
        }

        return $datas;
    }

    public function querySubParts($scope, $key, $datas)
    {
        $datas = $datas->{$key}(function ($query) use ($scope) {
            foreach ($scope as $key => $value) {
                foreach ($value as $_key => $_value) {
                    $scope_ = (array) $_value;  
                    $key_ = key($scope_);
                    $query = $this->queryParams($scope_, $key_, $query);
                }
            }
        });

        return $datas;
    }

    public function _get($id, $api) {
        $data = $api::find($id);
        return response()->json(['data' => $data],200);
    }

    public function _post(Request $request, $api) {
        $datas = [];        
        foreach ($request->data as $key => $values) {         
            $object = new $api();
            $object = $object::create($values);
            array_push($datas,$object);
        }
        return response()->json(['data' => $datas], 201);
    }

    public function _put(Request $request, $api) {
        $datas = [];
        
        foreach ($request->data as $key => $values) {
            $object = $api::find($values['id']);
            if(!$object) {
                $object = new $api();
                $object = $object::create($values);
            }else{                
                $object->update($values);
            }
            array_push($datas, $object);
        }      
        return response()->json(['data' => $datas], 201);
    }

    public function _delete(Request $request, $api) {        
        foreach ($request->data as $key => $value) {            
            $data = $api::find($value);
            $data->delete();
        }        
        return response()->json(['message' => 'Data deleted'],200);
    }

    public function _deleteAll($api) {                
        $api::truncate();     
        return response()->json(['message' => 'All Data deleted'],200);
    }
    
}
