<?php

namespace App\Http\Controllers\HR;

use App\Models\HR\EmployeeAttendance;
use App\Models\HR\Employee;
use App\Models\HR\TimeRecord;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;
use App\Http\Controllers\HR\EmployeeSalaryController;
use Carbon\Carbon;

class EmployeeAttendanceController extends BasedController
{

    private $api = EmployeeAttendance::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value) {
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        $datas = $request['data'][0];
        EmployeeAttendance::updateOrCreate(['date'=>$datas['date'], 'employee_id'=>$datas['employee_id']],$datas);
        return response()->json([], 201);
        //return $this->_post($request, $this->api);
    }

    public function record(Request $request, EmployeeSalaryController $es)
    {
        $validate = !empty($request->finger);
        $time = Carbon::parse($request->time)->format('H:i:s');
        $now = Carbon::now();
        $latest = TimeRecord::where('employee_id', $request->employee_id)->latest()->first();
        if (!is_null($latest)) {
            if ($now->diffInMinutes($latest->created_at) <= 30) {
                return response()->json(['data' => ['error' => true]]);
            }
        }
        $request['date'] = Carbon::parse($request->date)->format('Y-m-d');
        $request['time'] = $time;

        if (is_null((new EmployeeAttendance(['employee_id'=>$request->employee_id, 'date'=>$now->format('Y-m-d')]))->contract)) {
            return response()->json(['data' => ['noContract' => true]]);
        }

        $attendance = EmployeeAttendance::where('employee_id', $request->employee_id)->where('date', $now->copy()->format('Y-m-d'))->first();
        $yesterday = EmployeeAttendance::where('employee_id', $request->employee_id)->where('date', $now->subDay()->format('Y-m-d'))->where(function ($query) {
                                $query->whereNull('time_out')
                                      ->orWhere('time_in_2', null)
                                      ->orWhere('time_out_2', null);
                            })->first();
        if (!$attendance && $yesterday) {
            $time = Carbon::parse($time)->subDay();
            $_attendance = new EmployeeAttendance(['employee_id'=>$request->employee_id, 'date'=>$now->copy()->format('Y-m-d')]);
            if ($_attendance->schedule) {
                $end = $_attendance->schedule->end_time;
                $_end = Carbon::parse($end);
            }else {
                $_end = Carbon::parse('02:00:00');
            }
            if($_end->diffInHours($time) > (24 - 2)) $attendance = $yesterday;
        }
        if (is_null($attendance)) {
            if ($validate) {
                $attendance = EmployeeAttendance::create(['employee_id'=>$request->employee_id, 'date'=> date('Y-m-d'), 'time_in'=>$time]);
            }else {
                $attendance = new EmployeeAttendance(['employee_id'=>$request->employee_id, 'date'=> date('Y-m-d'), 'time_in'=>$time]);
            }
            $request['type'] = '1st time in';
        }else if(!is_null($attendance)){
            $_data = ['employee_id'=>$request->employee_id, 'date'=> date('Y-m-d')];
            if (is_null($attendance->time_out)) {
                $request['type'] = '1st time out';                
            }else if(is_null($attendance->time_in_2)){
                $request['type'] = '2nd time in';
            }else if(is_null($attendance->time_out_2)){
                $request['type'] = '2nd time out';
            }
            if (is_null($attendance->time_out_2)) {
                switch ($request['type']) {
                    case '1st time out':
                        $_data['time_out'] = $time;
                        break;
                    case '2nd time in':
                        $_data['time_in_2'] = $time;
                        break;
                    case '2nd time out':
                        $_data['time_out_2'] = $time;
                        break;
                }
                if ($validate) {
                 $attendance->update($_data);
                }else {
                    $attendance->time_out = !$attendance->time_out ? @$_data['time_out'] : $attendance->time_out;
                    $attendance->time_in_2 = !$attendance->time_in_2 ? @$_data['time_in_2'] : $attendance->time_in_2;
                    $attendance->time_out_2 = !$attendance->time_out_2 ? @$_data['time_out_2'] : $attendance->time_out_2;
                }
            }else {
                return response()->json(['data' => ['errorFull' => true]]);
            }
        }
        $employee = Employee::find($request->employee_id);
        if ($validate) {
            $data = TimeRecord::create($request->all());
            echo "<script>window.close();</script>";
        }else {
            $details = $es->computeAttendance([$attendance], $employee, true);
            //$details = [$employee, $attendance];
        }
        return response()->json(['data' => $details], 200);
    }

    public function records()
    {
        $date = Carbon::now();
        $dates = [$date->copy()->subDay()->format('Y-m-d'), $date->copy()->format('Y-m-d'), $date->copy()->subDay(2)->format('Y-m-d')];
        $data = TimeRecord::whereIn('date', $dates)->with('employee')->orderBy('id', 'desc')->get();
        return response()->json(['data' => $data], 200);
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) {
        return $this->_delete($request, $this->api);
    }
}
