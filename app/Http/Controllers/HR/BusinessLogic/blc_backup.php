<?php
namespace App\Http\Controllers\HR\BusinessLogic;

use App\Models\HR\EmployeeBenefit;
use App\Models\HR\EmployeeContract;
use App\Models\HR\EmployeeGovernmentId;
use App\Models\HR\EmployeeSchedule;
use DB;
use App\Models\Commons\Company;
use App\Models\Commons\Country;
use App\Models\Commons\Position;
use App\Models\Commons\Religion;
use App\Models\HR\Employee;
use App\Models\HR\EmployeeCharacterReference;
use App\Models\HR\EmployeeEducation;
use App\Models\HR\EmployeeEmploymentRecord;
use App\Models\HR\EmployeeRelation;
use App\Models\HR\EmployeeImage;
use App\Http\Controllers\BasedController;
use Illuminate\Http\Request;

class BusinessLogicController extends BasedController
{
    public function initEmployeeList() {
        $collections = (object)[];
        $collections->countries = Country::all();
        $collections->religions = Religion::all();
        $collections->companies = Company::all();
        $collections->positions = Position::all();

        return response()->json($collections,200);
    }

    public function recordContracts(Request $request) {
        DB::beginTransaction();
        try {
            $records = $request->data[0];            
            $datas = (object)[];
            //employee contracts
            $contracts = EmployeeContract::find($records['model']['id']);
            if(!$contracts) {
                $contracts = new EmployeeContract();
                $contracts = $contracts::create($records['model']);
            }else{
                $contracts->update($records['model']);
            }
            $datas->contracts = $contracts;

            //employee benefits
            $datas->benefits = [];
            foreach ($records['benefits'] as $key => $values) {                
                $benefits = EmployeeBenefit::find($values['id']);

                if(!$benefits) {
                    $benefits = new EmployeeBenefit();
                    $values['employee_contract_id'] = $contracts->id;
                    $benefits = $benefits::create($values);
                }else{
                    $benefits->update($values);
                }
                array_push($datas->benefits, $benefits);
            }

            //employee schedules
            $datas->schedules = [];
            foreach ($records['schedules'] as $key => $values) {
                $schedules = EmployeeSchedule::find($values['id']);
                $values['contract_id'] = $contracts->id;                    
                if(!$schedules) {
                    $schedules = new EmployeeSchedule();
                    $schedules = $schedules::create($values);
                }else{
                    $schedules->update($values);
                }
                array_push($datas->schedules, $schedules);
            }

            //employee governmentIds
            $datas->governmentIds = [];
            foreach ($records['governmentIds'] as $key => $values) {
                $governmentIds = EmployeeGovernmentId::find($values['id']);

                if(!$governmentIds) {
                    $governmentIds = new EmployeeGovernmentId();
                    $governmentIds = $governmentIds::create($values);
                }else{
                    $governmentIds->update($values);
                }
                array_push($datas->governmentIds, $governmentIds);
            }

            //trash
            if (array_key_exists('trash',$records)) {
                foreach ($records['trash']['schedules'] as $key => $values) {
                    EmployeeSchedule::find($values)->delete();
                }
                foreach ($records['trash']['benefits'] as $key => $values) {
                    EmployeeBenefit::find($values)->delete();
                }
                foreach ($records['trash']['governmentIds'] as $key => $values) {
                    EmployeeGovernmentId::find($values)->delete();
                }
            }

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            return response()->json(['error' => $e], 500);
            DB::rollback();
        }

        if ($success) {
            return response()->json(['data' => $datas], 201);
        }
    }

    public function cleanContracts(Request $request) {
        DB::beginTransaction();
            try {
                $Employee_id = 'employee_id';
                
                foreach ($request->data as $key => $value) {   
                    $temp = EmployeeContract::where('id', $value)->get();
                    $empId = json_decode($temp[0]->employee_id);
                        
                    EmployeeBenefit::where('employee_contract_id', $value)->delete();
                    EmployeeSchedule::where('contract_id', $value)->delete();
                    EmployeeGovernmentId::where($Employee_id, $empId)->delete();
                    EmployeeContract::where('id',$value)->delete();                    
                }

                DB::commit();
                $success = true;
            } catch (\Exception $e) {
                $success = false;
                return response()->json(['error' => $e], 500);
                DB::rollback();
            }

        if ($success) {
            return response()->json(['message' => 'Data deleted'],200);
        }            
    }

    public function record(Request $request) {
        DB::beginTransaction();
        try {
            $records = $request->data[0];
            $datas = (object)[];

            //Employee            
            $employee = Employee::find($records['employee']['id']);
            if(!$employee) {
                $employee = new Employee();
                $employee = $employee::create($records['employee']);
            }else{
                $employee->update($records['employee']);
            }
            $datas->employee = $employee;

            //Employee Image
            $image = EmployeeImage::find($records['image']['id']);
            $exploded = explode(',', $records['image']['image']);            
            if (sizeof($exploded) > 1) {
                $decoded = base64_decode($exploded[1]);
                $filename = $records['image']['name'];
                $path = public_path().'/'.$filename;
                $records['image']['image'] = $path;
                file_put_contents($path, $decoded);
            }
            
            if(!$image) {
                $image = new EmployeeImage();
                $records['image']['employee_id'] = $employee->id;
                $image = $image::create($records['image']);
            }else{
                $image->update($records['image']);
            }

            //Employee Relations
            $datas->relations = [];
            foreach ($records['relations'] as $key => $values) {
                $relations = EmployeeRelation::find($values['id']);

                if(!$relations) {
                    $relations = new EmployeeRelation();
                    $values['employee_id'] = $employee->id;
                    $relations = $relations::create($values);
                }else{
                    $relations->update($values);
                }
                array_push($datas->relations, $relations);
            }

            //Employee Educations

            $educations = EmployeeEducation::find($records['educations']['id']);
            if(!$educations) {
                $educations = new EmployeeEducation();
                $records['educations']['employee_id'] = $employee->id;
                $educations = $educations::create($records['educations']);
            }else{
                $educations->update($records['educations']);
            }
            $datas->educations = $educations;

            //Employee Employment Records
            $datas->employments = [];
            foreach ($records['employments'] as $key => $values) {
                $employments = EmployeeEmploymentRecord::find($values['id']);

                if(!$employments) {
                    $employments = new EmployeeEmploymentRecord();
                    $values['employee_id'] = $employee->id;
                    $employments = $employments::create($values);
                }else{
                    $employments->update($values);
                }
                array_push($datas->employments, $employments);
            }

            //Employee Character Reference
            $datas->references = [];
            foreach ($records['references'] as $key => $values) {
                $references = EmployeeCharacterReference::find($values['id']);

                if(!$references) {
                    $references = new EmployeeCharacterReference();
                    $values['employee_id'] = $employee->id;
                    $references = $references::create($values);
                }else{
                    $references->update($values);
                }
                array_push($datas->references, $references);
            }

            //trash
            if (array_key_exists('trash',$records)) {
                foreach ($records['trash']['relations'] as $key => $values) {
                    EmployeeRelation::find($values)->delete();
                }
                foreach ($records['trash']['employments'] as $key => $values) {
                    EmployeeEmploymentRecord::find($values)->delete();
                }
                foreach ($records['trash']['references'] as $key => $values) {
                    EmployeeCharacterReference::find($values)->delete();
                }
            }

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            return response()->json(['error' => $e], 500);
            DB::rollback();
        }

        if ($success) {
            return response()->json(['data' => $datas], 201);
        }
    }

    public function retrieve($id) {
        $records = (object)[];
        $key = 'employee_id';
        $records->biodata = Employee::where('id', $id)->with('religion', 'city', 'province', 'country', 'nationality')->get();
        $records->references = EmployeeCharacterReference::where($key,$id)->with('position', 'company')->get();
        $records->educations = EmployeeEducation::where($key,$id)->get();
        $records->employments = EmployeeEmploymentRecord::where($key,$id)->with('position', 'company')->get();
        $records->relations = EmployeeRelation::where($key,$id)->get();
        $records->image = EmployeeImage::where($key,$id)->get();

        return response()->json($records,200);
    }

    /*
        @params {
            $id = 'contract_id'
        }
    */

    public function retrieveContracts($id) {
        $records = (object)[];
        $records->contracts = [EmployeeContract::with('employee', 'contractType', 'position')->find($id)];
        $records->schedules = EmployeeSchedule::where('contract_id', $id)->with('schedule', 'day')->get();
        $records->benefits = EmployeeBenefit::where('employee_contract_id', json_decode($id))->with('benefit')->get();
        $records->governmentIds = EmployeeGovernmentId::where('employee_id', $records->contracts[0]->employee_id)->with('governmentId', 'employee')->get();
        
        return response()->json($records,200);
    }

    public function clean(Request $request) {
        DB::beginTransaction();
            try {
                $Employee_id = 'employee_id';
                foreach ($request->data as $key => $value) {                        
                    EmployeeCharacterReference::where($Employee_id,$value)->delete();
                    EmployeeEducation::where($Employee_id,$value)->delete();
                    EmployeeEmploymentRecord::where($Employee_id,$value)->delete();
                    EmployeeRelation::where($Employee_id,$value)->delete();
                    EmployeeImage::where($Employee_id,$value)->delete();
                    Employee::find($value)->delete();
                }

                DB::commit();
                $success = true;
            } catch (\Exception $e) {
                $success = false;
                return response()->json(['error' => $e], 500);
                DB::rollback();
            }

        if ($success) {
            return response()->json(['message' => 'Data deleted'],200);
        }            
    }

}