<?php

namespace App\Http\Controllers\HR;

use Illuminate\Http\Request;
use App\Models\HR\EmployeeSalary;
use App\Models\HR\EmployeeAttendance;
use App\Models\HR\EmployeeBenefit;
use App\Models\HR\EmployeeAdvanced;
use App\Models\HR\Employee;
use App\Models\HR\Schedule;
use App\Models\Commons\Day;
use App\Http\Controllers\BasedController;
use Carbon\Carbon;

class EmployeeSalaryController extends BasedController
{
    private $api = EmployeeSalary::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value) {
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        return $this->_post($request, $this->api);
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) {
        return $this->_delete($request, $this->api);
    }


    //getContract Hiredates

    //gettheirCutoffs

    //getSchedules

    //compute hours with sanctions

    //calculate per hour rate

    //only 1 time record leads to AWOL

    //add benefits

    //deduct advances

    public function generate(Request $request)
    {
        $data = $request['data'][0];
        $month = Carbon::parse($data['month']);
        $month_end = $month->copy()->endOfMonth()->format('Y-m-d');

        $employee = Employee::with('contracts')->find($data['employee_id']);

        EmployeeSalary::where('employee_id', $employee->id)->where(function($query) use ($data) { $query->where('period_start','like', '%'.$data['month'].'%')->orWhere('period_end', 'like', '%'.$data['month'].'%'); })->delete();
        $attendances = EmployeeAttendance::where('employee_id', $data['employee_id'])->where('date', 'like', '%'.$data['month'].'%')->orderBy('date')->get();

        $attendance_data = $this->computeAttendance($attendances, $employee);
        $attendance_values = $attendance_data['values'];
        $cutoffs = $attendance_data['cutoffs'];
        $advances = [];
        $advances_data = [];
        $deductions = [];
        $deductions_data = [];

        foreach (collect($cutoffs)->unique() as $value) {
            $start = Carbon::parse($value['start']);
            $end = $value['end'];
            $key = $value['contract_id']."|".$value['start']."|".$value['end'];
            while ($start->copy()->format('Y-m-d') !== Carbon::parse($end)->addDay()->format('Y-m-d')) {
                $a = EmployeeAdvanced::where('employee_id', $employee->id)->where('effect_date', $start->copy()->format('Y-m-d'))->with('details.inventory.item')->get();

                foreach ($a as $val) {
                    $advances_data[$key][] = $val->toArray();
                    $advances[$key][] = $val->total;
                }

                $b = EmployeeAdvanced::where('employee_id', $employee->id)->where('pay_date', $start->copy()->format('Y-m-d'))->with('details.inventory.item')->get();
                foreach ($b as $val) {
                    $deductions_data[$key][] = $val->toArray();
                    $deductions[$key][] = $val->total;
                }
                $start = $start->copy()->addDay();
            }
        }
        $employee_salaries = [];
        foreach ($attendance_values as $key => $value) {
            $_key_ = explode("|", $key);
            $_benefits = EmployeeBenefit::with('benefit')->where('employee_contract_id', $_key_[0])->get();
            $benefits = $_benefits->pluck('benefit.value', 'benefit.name');
            $frequency = (int)($value['frequency']);
            $b_total = $benefits->sum() / $frequency;
            $b_deducted = $benefits->map(function($benefit) use ($frequency){
                return $benefit / $frequency;
            });
            $value['dates']['benefits'] = $b_deducted;
            $value['dates']['advances'] = @$advances_data[$key];
            $value['dates']['deductions'] = @$deductions_data[$key];
            $_advances = (float) array_sum((array) @$advances[$key]);
            $_deductions = (float) array_sum((array) @$deductions[$key]);
            EmployeeSalary::create([
                'employee_id'=>$value['employee_id'],
                'contract_id'=>$_key_[0],
                'period_start' => $_key_[1],
                'period_end'=>$_key_[2],
                'hours' => $value['hours'],
                'basic' => $value['basic'],
                'tardy' => $value['tardy'],
                'benefits' => $b_total,
                'advances' => $_advances,
                'deductions' => $_deductions,
                'total' => $value['basic'] + $b_total + (float) $_advances - (float) $_deductions,
                'data' => json_encode($value['dates'])
            ]);
        }
        return response()->json(['data' => 'success'], 201);
    }

    public function computeAttendance($attendances, $employee, $date_only = false)
    {
        $attendance_values = [];
        $cutoffs = [];
        $_hours_ = null;
        foreach ($attendances as $key => $attendance) {
            $date = Carbon::parse($attendance->date);
            $contract = @$attendance->contract;
            $hours = [];
            if ($contract) {
                $daily_rate = ($contract->salary_per_month * 12) / 365;
                if ($attendance->schedule && $attendance->day) {
                    $hours = $this->computeHour(@$attendance->toArray(),$daily_rate);
                }
                if(empty($hours)){
                    $hours = $this->computeHour(@$attendance->toArray(), $daily_rate);
                }
                if ($date_only) {
                    $_hours_ = $hours; 
                }
                $cutoff = $contract->cutoff;
                switch ($cutoff->frequency) {
                    case 'Monthly':
                        $start_cut = $date->copy()->startOfMonth()->format('Y-m-d'); 
                        $end_cut = $date->copy()->endOfMonth()->format('Y-m-d');
                        $frequency = 1;
                        break;
                    case 'Semi-Monthly':
                        $dates = $this->getSemiMonth($date);
                        $start_cut = $dates[0]; 
                        $end_cut = $dates[1];
                        $frequency = 2;
                        break;
                    case 'Weekly':
                        $start_cut = $date->copy()->startOfWeek()->format('Y-m-d'); 
                        $end_cut = $date->copy()->endOfWeek()->format('Y-m-d');
                        $frequency = 4;
                        break;
                }
                $cutoffs[] = ['contract_id' => $contract->id, 'start' => $start_cut, 'end' => $end_cut];

                $hours['contract_id'] = $contract->id;
                $hours['period_start'] = $start_cut;
                $hours['period_end'] = $end_cut;
                $hours['daily_rate'] = $daily_rate;
                $hours['basic'] = $hours['basic_pay'];

                $_key_ = $contract->id."|".$start_cut."|".$end_cut;
                $attendance_values[$_key_]['dates'][] = $hours;
                $attendance_values[$_key_]['employee_id'] = $attendance->employee_id;
                $attendance_values[$_key_]['frequency'] = $frequency;
                $attendance_values[$_key_]['basic'] = @$attendance_values[$_key_]['basic'] +$hours['basic'];
                $attendance_values[$_key_]['hours'] = @$attendance_values[$_key_]['hours'] +$hours['hours'];
                $attendance_values[$_key_]['tardy'] = @$attendance_values[$_key_]['tardy'] +$hours['late'];
            }
        }
        if ($date_only) {
            if ($contract) {
                return $_hours_;
            }else {
                return (object) ['attendance' => $attendance];
            }
        }
        return ['values' => $attendance_values, 'cutoffs' => $cutoffs];
    }

    public function computeHour($attendance, $daily_rate = null)
    {
        $attendance = new EmployeeAttendance($attendance);
        $schedule = $attendance->schedule;
        $day = $attendance->day;
        $_attendance = [
            $attendance->time_in,
            $attendance->time_out,
            $attendance->time_in_2,
            $attendance->time_out_2
        ];
        $_attendance = array_values(array_filter($_attendance));
        $time = [
            @strtotime($attendance->time_in),
            @strtotime($attendance->time_out),
            @strtotime($attendance->time_in_2),
            @strtotime($attendance->time_out_2)
        ];
        $time = array_values(array_filter($time));
        $start_time = date('H:i', $time[0]);
        $end_time = date('H:i', end($time));
        $no_schedule = false;

        if (empty($schedule)) {
            $schedule = new Schedule([
                'start_time' => $_attendance[0],
                'end_time' => end($_attendance),
                'break_hour' => 2,
                'break_minute' => null,
                'late_hour' => 0,
                'late_minute' => null,
                'late_deduction_hour' => 0,
                'late_deduction_minute' => null,
                'nobreak_sanction' => 0,
                'halfday_sanction' => 0,
                'absent_sanction' => 1
            ]);
            $no_schedule = true;
        }

        $sched = [
            @strtotime($schedule->start_time),
            @strtotime($schedule->end_time)
        ];

        $breaktime = $schedule->breaktime;
        $_late = $this->timeDifference($start_time, $schedule->start_time);
        $late = $_late < 0 ? abs($_late) : 0;
        $_end_late = $this->timeDifference($schedule->end_time, $end_time); 
        $end_late =  $_end_late < 0 ? abs($_end_late) : 0;
        $mid_late = 0;
        $grace_period = 0.2;
        $halfday = false;
        $absent = false;
        $nobreak = false;

        $sanction_nobreak = (bool)$schedule->nobreak_sanction;
        $sanction_halfday = (bool)$schedule->halfday_sanction;
        $sanction_absent = (bool)$schedule->absent_sanction;

        $late_meter = $this->hourMinToHour($schedule->late_hour, $schedule->late_minute);
        $late_deduction = $this->hourMinToHour($schedule->late_deduction_hour, $schedule->late_deduction_minute);

        $total_hours = $this->timeDifference($schedule->start_time, $schedule->end_time);
        if ($total_hours < 0) {
            $total_hours = $total_hours + 24;
        }
        $total_attendance = $this->timeDifference($start_time, $end_time);
        if ($total_attendance < 0) {
            $total_attendance = $total_attendance + 24;
        }
        //var_dump($total_attendance/2, $total_hours/2, $attendance->date);
        if ($total_attendance/2 > $total_hours/2 || count($_attendance) == 4 || ($no_schedule && $total_hours > 7)) {
            $deducted_th = $total_hours - $schedule->breaktime;
        }else {
            $deducted_th = $total_hours;
        }

        $deducted_th = $deducted_th < 0 ? 0 : $deducted_th;

        if ($late_deduction && $late > $grace_period) {
            $breaktime = $breaktime - $late_deduction;
        }

        if ($sanction_nobreak && $late > $late_meter) {
            $nobreak = true;
            $breaktime = 0;
        }

        if (count($_attendance) < 4) {
            $mid_day_break = 0;
        }else {
            $mid_day_break = $this->timeDifference($attendance->time_out, $attendance->time_in_2); 
        }

        if ($sanction_absent && (empty(@$time[2]) || empty(@$time[3])) && $mid_day_break > $breaktime) {
            $absent = true;
            $hours = 0;
        }else{
            if ($sanction_halfday && $mid_day_break > $breaktime) {
                // check if breaktime - timediff of time 2 and 3 and has negative value time other half will be blanks
                $halfday = true;
                unset($time[2]);
                unset($time[3]);
            }else {
                $break_ = '';
                if (count($_attendance) === 4) {
                    $break_ = ($time[1] + ($breaktime *60*60));
                    $time[2] = $time[2] < $break_ ? $break_ : $time[2];
                    $break_ = $time[2]." -- ".$break_;
                    $mid_late = ($mid_day_break > $breaktime) ? $mid_day_break - $breaktime : 0;
                } else if (count($_attendance) === 2) {
                    $time[1] = $time[1] - ($breaktime *60*60);
                }
            }
            //$_hours = $this->countTime($time, $sched, $schedule->breaktime);
            $_hours = $deducted_th - ($late + $end_late + $mid_late);
            $hours = $deducted_th < $_hours ? $deducted_th : $_hours;
            $hours = $hours < 0 ? 0 : $hours; 
            if (count($_attendance) == 1) {
                $hours = 0;
            }
        }
        $basic_dividend = $no_schedule ? 14 : $deducted_th;
        $basic_pay = $deducted_th > 0 ? ($daily_rate/$basic_dividend) * $hours : 0;
        $basic_pay = $basic_pay > $daily_rate ? $daily_rate : $basic_pay;

        //late deduction on 4 time records

        //count hour on 2 time records

        return [
            'show' => false,
            'attendance' => $attendance,
            'schedule' => $schedule,
            'day' => $day,
            'daily_rate' => $daily_rate,
            'basic_pay' => $basic_pay,
            'hours' => $hours,
            'total_hours' => $deducted_th,
            'late' => $late,
            'mid_late' => $mid_late,
            'end_late' => $end_late,
            'late_meter' => $late_meter,
            'late_deduction' => $late_deduction,
            'breaktime' => $breaktime,
            'mid_day_break' => $mid_day_break,
            'halfday' => $halfday,
            'absent' => $absent,
            'nobreak' => $nobreak,
            'no_schedule' => $no_schedule,
        ];
    }

    public function hourMinToHour($hour, $min)
    {
        return (int)$hour + ((int)$min / 60);
    }

    public function timeDifference($min, $max) 
    {
      $a = strtotime(Carbon::parse($min)->format('H:i'));
      $b = strtotime(Carbon::parse($max)->addDay()->format('H:i'));
      $hour = ($b - $a) / 60 / 60;
      return $hour >= 24 ? $hour - 24 : $hour;
    }

    public static function getSemiMonth($date, $day = true)
    {
        $month = Carbon::parse($date);

        $first_1 = $month->copy()->startOfMonth();
        $first_2 = $month->copy()->startOfMonth()->addDay(14);
        $first = [$first_1->format('Y-m-d'), $first_2->format('Y-m-d')];

        $second_1 = $first_2->copy()->addDay();
        $second_2 = $month->copy()->endOfMonth();
        $second = [$second_1->format('Y-m-d'), $second_2->format('Y-m-d')];

        if ($day) {
            if($month->format('d') <= 15){
                return $first;
            }
            return $second;
        }

        return [$first, $second];
    }

    public function sample($value='')
    {
        return $value . " asdasd";
    }
}
    /*public function countTime($time, $schedule, $break)
    {
        $sm_break_time = 0; $hours = 0; $smTime = 0;
        $smTime_str = 0; $timeDiff = 0; $n_index = 0;

        $filtered = array_values(array_filter($this->timeParser($time, $schedule)));
        foreach ($filtered as $key => $value) {
            ${'time'.($key+1)} = $value;
        }
        $endTime = ${'time'.count($filtered)};
        if ($time1 > $endTime) {
            $endTime += 24 * 60 * 60;
        }
        $timeDiff = $time1 - $endTime;

        if (count($filtered) > 2) {
            $smTimeDiff = $time2 - $time3;
            $smTime_str = (abs($smTimeDiff)/60)/60;
            $smTime = $smTime_str > $sm_break_time ? $sm_break_time : $smTime_str;
            if (count(array_filter($schedule)) > 0 && !$this->knowInSchedule([$time2, $time3], $schedule)) {
                $smTime = 0;
            }
            $timeDiff= $time1 - $time2;
            if (count($filtered) == 4) {
                $timeDiff=abs($time1-$time2) + abs($time3-$time4);
            }
        }
        $n_index = count($filtered)-1;
            
        $hours = ((abs($timeDiff)/60)/60) + $smTime;     
        
        return $hours;
    }

    public function timeParser($time, $schedule = [],$type = "")
    {
        $vars = $time;
        if (count($schedule)>0) {
            foreach ($vars as $key => $value) {
                if (!empty($value)) {
                    if ($value < ($schedule[0])) {
                        $vars[$key] = ($schedule[0]);
                    }
                    if ($value > ($schedule[1])) {
                        $vars[$key] = ($schedule[1]);
                    }
                }
            }
        }
        return $vars;
    }

    public function knowInSchedule($time, $sched)
    {
        $early = strtotime("06:00");
        if ($time[0] < $early) {
            $time[0] += 24 * 60 * 60;
        }
        if ($time[1] > $time[1]) {
            $time[1] += 24 * 60 * 60;
        }
        if ($time[0] < ($sched[0]) || $time[1] > ($sched[1])) {
            return false;
        }
        return true;
    }*/
