<?php

namespace App\Http\Controllers\HR;

use App\Models\HR\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;
use App\Http\Controllers\HR\EmployeeAttendanceController;
use App\Http\Controllers\HR\EmployeeSalaryController;
use App\Fingerprint;

class EmployeeController extends BasedController
{
    private $api = Employee::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value) {
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        return $this->_post($request, $this->api);
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) {
        return $this->_delete($request, $this->api);
    }


    public function register(Request $request)
    {
        $fingerprint = new Fingerprint();
        $repo = $fingerprint->repo;
        $base = $fingerprint->base;
        echo $request->user_id.";SecurityKey;12;".$base."reg.php;".$repo."device.php";
    }

    public function recordFinger(Request $request)
    {
        $data       = explode(";",$request->data);
        $vStamp     = $data[0];
        $sn         = $data[1];
        $user_id    = $data[2];
        $regTemp    = $data[3];
        var_dump($data);
        $q = Fingerprint::updateOrCreate(['user_id' => $user_id], ['data' => $regTemp]);
        if ($q) {
            echo "<script>window.close();</script>";
        }
    }

    public function address()
    {
        return \App\Repositories\Check::address();
    }


    public function check(Request $request)
    {
        $fingerprint = Fingerprint::where('user_id', $request->user_id)->first();
        $repo = $fingerprint->repo;
        $base = $fingerprint->base;

        echo $request->user_id.";".$fingerprint->data.";SecurityKey;10;".$base."check.php;".$repo."getac.php".";extraParams";
    }

    public function checkFinger(Request $request, EmployeeAttendanceController $ea, EmployeeSalaryController $es)
    {
        $data       = explode(";",$request->data);
        $vStamp     = $data[1];
        $sn         = $data[3];
        $request['employee_id'] = $data[0];
        $request['time'] = $data[2];
        $request['data'] = date('Y-m-d');
        $request['finger'] = true;

        $ea->record($request, $es);
    }

    public function fingernote(Request $request)
    {
        $datas = $request['data'][0];
        Fingerprint::where('user_id', $datas['user_id'])->update(['notes' => $datas['notes']]);

        return response()->json(['data' => 'success'], 201);
    }

   
}
