<?php

namespace App\Http\Controllers\HR;

use App\Models\HR\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;

class ScheduleController extends BasedController
{
    private $api = Schedule::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value) {
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        return $this->_post($request, $this->api);
        //$datas = $request['data'][0];
        /*$datas['start_time'] = \Carbon\Carbon::parse($datas['start_time'])->format('H:i:s');
        $datas['end_time'] = \Carbon\Carbon::parse($datas['end_time'])->format('H:i:s');*/
        Schedule::create($datas);     
        return response()->json([], 201);
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
        $datas = $request['data'][0];
        $schedule = Schedule::find($datas['id']);
        /*$datas['start_time'] = \Carbon\Carbon::parse($datas['start_time'])->format('H:i:s');
        $datas['end_time'] = \Carbon\Carbon::parse($datas['end_time'])->format('H:i:s');*/
        $schedule->update($datas);
        return response()->json([], 201);
    }

    public function delete(Request $request) {
        return $this->_delete($request, $this->api);
    }
}
