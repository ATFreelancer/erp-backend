<?php

namespace App\Http\Controllers\HR;

use App\Models\HR\EmployeeContract;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;
use App\Fingerprint;

class EmployeeContractController extends BasedController
{
    private $api = EmployeeContract::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value) {
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        return $this->_post($request, $this->api);
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }
    
    public function delete(Request $request) {
        return $this->_delete($request, $this->api);
    }
}
