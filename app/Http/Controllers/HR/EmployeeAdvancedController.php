<?php

namespace App\Http\Controllers\HR;

use App\Models\HR\EmployeeAdvanced;
use App\Models\HR\EmployeeAdvancedDetail;
use App\Models\Inventory\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;

class EmployeeAdvancedController extends BasedController
{
    private $api = EmployeeAdvanced::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value) {
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        $data = $request['data'][0];
        $advance = EmployeeAdvanced::updateOrcreate(['id'=>@$data['id']], [
            'employee_id' => $data['employee_id'],
            'type' => $data['type'],
            'effect_date' => $data['effect_date'],
            'pay_date' => $data['pay_date']
        ]);

        if ($advance->details->count() > 0) {
            $advance->details()->delete();
        }
        foreach ($data['employee_advanced_details'] as $key => $value) {
            if (!empty($value['inventory_id'])) {
                $value['cash'] = Inventory::find($value['inventory_id'])->item->unit_price * $value['quantity'];
            }
            EmployeeAdvancedDetail::create([
                'employee_advanced_id' => $advance->id,
                'inventory_id' => @$value['inventory_id'],
                'quantity' => @$value['quantity'],
                'cash' => @$value['cash'],
                'description' => @$value['description']
            ]);
        }

        $adv = EmployeeAdvanced::find($advance->id);
        $adv->update(['total'=> $adv->details->pluck('cash')->sum()]);

        return response()->json(['data' => $data], 201);
        //return $this->_post($request, $this->api);
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) {
        return $this->_delete($request, $this->api);
    }
}
