<?php

namespace App\Http\Controllers\Account;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;

class AccountController extends BasedController
{
    private $api = User::class;
    
    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value){
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {        
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
    	$datas = $request['data'][0];
    	if(User::where('username',$datas['username'])->exists()){
    		return response()->json(['error'=>'Username already exists!']);
    	}
        $datas['status'] = '0';
        $datas['password'] = bcrypt($datas['username']);
        $datas['api_token'] = str_random(60);
        User::create($datas);

        return response()->json(['data' => $datas], 201);
    }

    public function put(Request $request) {
        $datas = $request['data'][0];
        $user = User::find($datas['id']);
        $user->assignRoles($datas['roles']);
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) {        
        return $this->_delete($request, $this->api);
    }

    public function change(Request $request)
    {
        $datas = $request['data'][0];
        $user = User::find($datas['id']);
        if (!\Hash::check($datas['oldpassword'], $user->password)) {
            return response()->json(['error'=>'Old password is incorrect!']);
        }
        $exist = User::where('username',$datas['username'])->first();
        if($exist && @$exist->id != $user->id){
            return response()->json(['error'=>'Username already exists!']);
        }
        $datas['password'] = bcrypt($datas['password']);
        $user->update($datas);

        return response()->json([],201);
    }

    public function reset(Request $request)
    {
        $datas = $request['data'][0];
        $user = User::find($datas);
        $user->password = bcrypt($user->username);
        $user->save();

        return response()->json([],201);
    }

    public function disable(Request $request)
    {
        $datas = $request['data'][0];
        $user = User::find($datas);
        $user->status = $user->status === '0' ? '1' : '0';
        $user->save();

        return response()->json([],201);
    }
}
