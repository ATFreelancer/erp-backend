<?php

namespace App\Http\Controllers\Account;

use App\Models\Account\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;

class RolesController extends BasedController
{
    private $api = Role::class;
    
    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value){
        return $this->_getBy($key, $value, $this->api);
    }

    public function get($id) {        
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        return $this->_post($request, $this->api);        
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) { 
        if (\DB::table('role_user')->where('role_id', $request['data'])->exists()) {
            return response()->json(['error'=>'Cannot delete role.']);
        }
        return $this->_delete($request, $this->api);
    }
}
