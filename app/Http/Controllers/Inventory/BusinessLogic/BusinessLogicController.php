<?php
namespace App\Http\Controllers\Inventory\BusinessLogic;

use DB;
use App\Models\Commons\Affiliation;
use App\Models\Commons\Company;
use App\Models\Commons\Packing;
use App\Models\Commons\Unit;
use App\Models\Inventory\Inventory;
use App\Models\Inventory\ItemCategory;
use App\Models\Inventory\ItemTransfer;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;

class BusinessLogicController extends BasedController
{
    public function initItemsConfig() {
        $collections = (object)[];
        $collections->units = Unit::all();
        $collections->companies = Company::all();
        $collections->packings = Packing::all();
        $collections->item_categories = ItemCategory::all();

        return response()->json($collections,200);
    }

    public function initPriceListConfig() {
        $collections = (object)[];
        $collections->affiliations = Affiliation::all();
        $collections->item_categories = ItemCategory::all();

        return response()->json($collections,200);
    }

    public function transact(Request $request) {        
        DB::beginTransaction();
        try {
            $itemTransferCollection = $request->data;
            $datas = [];  
            foreach ($itemTransferCollection as $key => $value) {                       
                //update inventory quantity ex: candy = 100 - 10 = 90
                $inventoryFrom = Inventory::find($value['inventory_id']);
                $inventoryFrom->quantity = $inventoryFrom->quantity - $value['quantity'];
                $inventoryFrom->save();       
                //transfer 10 candies (create or update)                           
                $object = Inventory::where('affiliation_id', $value['affiliation_receiver_id'])
                ->where('item_id', $inventoryFrom->item_id)
                ->get();                      
                if($object->isEmpty()) {                                        
                    $temp = array('item_id' => $inventoryFrom->item_id, 'quantity' => $value['quantity'], 'affiliation_id' => $value['affiliation_receiver_id']);
                    $object = new Inventory();                    
                    $object = $object::create($temp);
                }else{             
                    foreach ($object as $key => $val) {                        
                        var_dump($val->quantity);
                        $val->quantity = $val->quantity + $value['quantity'];
                        $val->save();
                    }                                          
                    
                }    
                //create item transfer data in database
                $itemTransfer = new ItemTransfer();                    
                $itemTransfer = $itemTransfer::create($value);
                array_push($datas, $itemTransfer);
            }
            
            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            return response()->json(['error' => $e], 500);
            DB::rollback();
        }

        if ($success) {
            return response()->json(['data' => $datas], 201);
        }
    }
}