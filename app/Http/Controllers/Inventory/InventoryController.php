<?php

namespace App\Http\Controllers\Inventory;

use App\Models\Inventory\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\BasedController;

class InventoryController extends BasedController
{
    private $api = Inventory::class;

    public function getAll(Request $request) {
        return $this->_getAll($request, $this->api);
    }

    public function getBy($key, $value){
        //$scopes = [ 'withRelations' => ['item', 'item.packing','item.itemCategory', 'item.unit', 'item.company']];

        return $this->_getBy($key, $value, $this->api);
    }

    public function saveAll(Request $request)
    {
        $datas = $request->data[0];
        $recounts = collect($datas['recounts'])->filter(function ($i) { return is_numeric($i['recount']); });
        $items = Inventory::where('affiliation_id', $datas['store'])->whereIn('id', $recounts->keys())->get();
        $recounts = $recounts->toArray();
        foreach ($items as $key => $value) {
            $recounts[$value->id]['undo'] = $value->quantity;
            $value->update(['quantity' => $recounts[$value->id]['recount']]);
        }
        return response()->json(['recounts' => $recounts], 201);
    }

    public function get($id) {        
        return $this->_get($id, $this->api);
    }

    public function post(Request $request) {
        return $this->_post($request, $this->api);        
    }

    public function put(Request $request) {
        return $this->_put($request, $this->api);
    }

    public function delete(Request $request) {        
        return $this->_delete($request, $this->api);
    }
}
