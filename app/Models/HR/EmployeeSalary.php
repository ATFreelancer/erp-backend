<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends Model
{
    protected $fillable = ['employee_id','contract_id', 'period_start','period_end','hours','basic','tardy','restdays','holidays','benefits','advances', 'deductions', 'total','data'];

    public function contract()
    {
    	return $this->belongsTo('App\Models\HR\EmployeeContract', 'contract_id');
    }
}
