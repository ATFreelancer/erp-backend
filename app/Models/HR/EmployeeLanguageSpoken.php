<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeLanguageSpoken extends Model
{
    protected $fillable = ['employee_id','language_id'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }

    public function language() {
        return $this->belongsTo('App\Models\Commons\Language');
    }
}
