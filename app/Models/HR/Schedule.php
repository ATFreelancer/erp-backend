<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['name','start_time','end_time','break_hour','break_minute', 'late_hour', 'late_minute', 'late_deduction_hour', 'late_deduction_minute', 'nobreak_sanction', 'halfday_sanction', 'absent_sanction', 'description'];

    public function setStartTimeAttribute($value)
    {
    	$this->attributes['start_time'] = $value ? \Carbon\Carbon::parse($value)->format('H:i:s') : null;
    }

    public function setEndTimeAttribute($value)
    {
    	$this->attributes['end_time'] = $value ? \Carbon\Carbon::parse($value)->format('H:i:s') : null;
    }

    public function getBreakTimeAttribute()
    {
    	return $this->break_hour + ($this->break_minute / 60);
    }
}
