<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeImage extends Model
{
    protected $fillable = ['employee_id','image','name','type','size'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }
}
