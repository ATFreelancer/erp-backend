<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeAttendance extends Model
{
    protected $fillable = ['employee_id','date','time_in','time_out','time_in_2','time_out_2'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }
    public function setTimeInAttribute($value)
    {
    	$this->attributes['time_in'] = $value ? \Carbon\Carbon::parse($value)->format('H:i:s') : null;
    }
    public function setTimeOutAttribute($value)
    {
    	$this->attributes['time_out'] = $value ? \Carbon\Carbon::parse($value)->format('H:i:s') : null;
    }

    public function setTimeIn2Attribute($value)
    {
    	$this->attributes['time_in_2'] = $value ? \Carbon\Carbon::parse($value)->format('H:i:s') : null;
    }

    public function setTimeOut2Attribute($value)
    {
    	$this->attributes['time_out_2'] = $value ? \Carbon\Carbon::parse($value)->format('H:i:s') : null;
    }

    public function getContractAttribute()
    {
        $date = $this->date;
        return $this->employee->contracts()->where('hire_date', '<=', $this->date)->where(function($query) use ($date){
            $query->whereNull('end_date')
                  ->orWhere('end_date', '>=', $date);
        })->latest()->first();
    }

    public function getScheduleAttribute()
    {
        $schedules = $this->contract->schedules()->with('schedule','day')->get();
        if ($this->day) {
            return $schedules->where('day.id', $this->day->id)->sortByDesc('updated_at')->first()->schedule;
        }
        return null;
    }

    public function getDayAttribute()
    {
        $schedules = $this->contract->schedules()->with('schedule','day')->get();

        $date = \Carbon\Carbon::parse($this->date);
        $_date = $this->date;
        $daysOfWeek = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
        $date_day = $daysOfWeek[$date->copy()->dayOfWeek];
        $day_every = "every_". $daysOfWeek[$date->copy()->dayOfWeek];
        
        $days_id = $schedules->pluck('day.id')->toArray();

        $day = \App\Models\Commons\Day::where('start_date', '<=', $this->date)
                ->where(function($query) use ($_date){
                    $query->whereNull('end_date')
                          ->orWhere('end_date', '>=', $_date);
                })
                ->where(function ($query) use ($day_every) {
                    $query->where(function($_query) {
                        $_query->where('every_mon', 0)
                                ->where('every_tue', 0)
                                ->where('every_wed', 0)
                                ->where('every_thu', 0)
                                ->where('every_fri', 0)
                                ->where('every_sat', 0)
                                ->where('every_sun', 0);
                    })
                    ->orWhere($day_every, 1);
                })
                ->latest()->find($days_id)->first();
        return $day;
    }
}
