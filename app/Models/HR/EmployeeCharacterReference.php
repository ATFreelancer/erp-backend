<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeCharacterReference extends Model
{
    protected $fillable = ['employee_id','first_name','middle_name','last_name','suffix','company_id','position_id',
        'contact_number','email_address'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }

    public function company() {
        return $this->belongsTo('App\Models\Commons\Company');
    }

    public function position() {
        return $this->belongsTo('App\Models\Commons\Position');
    }

}
