<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeContract extends Model
{
    protected $fillable = ['employee_id','position_id','salary_per_month','contract_type_id','hire_date','regularization_duedate', 'cutoff_id', 'end_date'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }

    public function schedules() {
        return $this->hasMany('App\Models\HR\EmployeeSchedule', 'contract_id');
    }    

    public function contractType() {
        return $this->belongsTo('App\Models\HR\ContractType');
    }

    public function position() {
        return $this->belongsTo('App\Models\Commons\Position');
    }

    public function cutoff() {
        return $this->belongsTo('App\Models\HR\Cutoff');
    }

    public function salaries() {
        return $this->hasMany('App\Models\HR\EmployeeSalary', 'contract_id');
    }
}
