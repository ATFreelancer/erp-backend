<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeBenefit extends Model
{
    protected $fillable = ['employee_contract_id','benefit_id'];

    public function employeeContract() {
        return $this->belongsTo('App\Models\HR\EmployeeContract');
    }

    public function benefit() {
        return $this->belongsTo('App\Models\HR\Benefit');
    }
}
