<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class TimeRecord extends Model
{
    protected $fillable = ['employee_id', 'date', 'type', 'time'];
    protected $with = ['employee'];
    
    public function employee()
    {
    	return $this->belongsTo('App\Models\HR\Employee');
    }
}
