<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeEducation extends Model
{
    protected $fillable = ['employee_id','elementary','highschool','college','elementary_grad_year',
        'highschool_grad_year','college_grad_year','graduate_school_grad_year','graduate_school','degree'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }
}
