<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmploymentRecord extends Model
{
    protected $fillable = ['employee_id','company_id','position_id','year_from','year_to'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }

    public function company() {
        return $this->belongsTo('App\Models\Commons\Company');
    }

    public function position() {
        return $this->belongsTo('App\Models\Commons\Position');
    }
}
