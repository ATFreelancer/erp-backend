<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{
    protected $fillable = ['name','description'];
}
