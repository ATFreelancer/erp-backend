<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeAdvancedDetail extends Model
{
    protected $fillable = ['employee_advanced_id','inventory_id','quantity','cash','description'];

    public function employeeAdvanced() {
        return $this->belongsTo('App\Models\HR\EmployeeAdvanced');
    }

    public function inventory() {
        return $this->belongsTo('App\Models\Inventory\Inventory');
    }
}
