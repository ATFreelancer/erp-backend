<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['first_name','middle_name','last_name','suffix','birthdate','nationality_id','religion_id','address','city_id','province_id','country_id','contact_number','email_address',
        'height','weight'];

    protected $with = ['image', 'contract.position', 'fingerprint'];

    public function contract()
    {
        return $this->hasOne('App\Models\HR\EmployeeContract')->latest();
    }

    public function contracts()
    {
        return $this->hasMany('App\Models\HR\EmployeeContract');
    }

    public function attendances()
    {
        return $this->hasMany('App\Models\HR\EmployeeAttendance');
    }

    public function fingerprint()
    {
        return $this->hasOne('App\Fingerprint', 'user_id');
    }

    public function nationality() {
        return $this->belongsTo('App\Models\Commons\Country');
    }

    public function religion() {
        return $this->belongsTo('App\Models\Commons\Religion');
    }    

    public function city() {
        return $this->belongsTo('App\Models\Commons\City');
    }

    public function province() {
        return $this->belongsTo('App\Models\Commons\Province');
    }

    public function country() {
        return $this->belongsTo('App\Models\Commons\Country');
    }

    public function image() {
        return $this->hasOne('App\Models\HR\EmployeeImage');
    }    

    public function scopeWithRelations($query, $params = [])
    {
        return $query->with($params);
    }

    public function timerecords()
    {
        return $this->hasMany('App\Models\HR\TimeRecord', 'employee_id')->where('date', date('Y-m-d'))->orWhere('date', date('Y-m-d', strtotime( '-1 days' )));
    }
}
