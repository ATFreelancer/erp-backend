<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeRelation extends Model
{
    protected $fillable = ['employee_id','first_name','middle_name','last_name','suffix','relation_type','birthdate',
        'occupation','address','city_id','province_id','country_id'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }

    public function city() {
        return $this->belongsTo('App\Models\Commons\City');
    }

    public function province() {
        return $this->belongsTo('App\Models\Commons\Province');
    }

    public function country() {
        return $this->belongsTo('App\Models\Commons\Country');
    }

    public function scopeWithRelations($query, $params = [])
    {
        return $query->with($params);
    }
}
