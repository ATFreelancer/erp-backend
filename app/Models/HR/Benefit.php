<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $fillable = ['name','value','description'];
}
