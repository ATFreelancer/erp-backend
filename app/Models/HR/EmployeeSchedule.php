<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeSchedule extends Model
{
    /*protected $fillable = ['employee_id','schedule_id','day_id','week_number'];*/
    protected $fillable = ['contract_id','schedule_id','day_id','week_number'];

    /*public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }*/

    public function employee() {
        return $this->contract->employee;
    }

    public function contract() {
        return $this->belongsTo('App\Models\HR\EmployeeContract', 'contract_id');
    }

    public function schedule() {
        return $this->belongsTo('App\Models\HR\Schedule');
    }

    public function day() {
        return $this->belongsTo('App\Models\Commons\Day');
    }
}
