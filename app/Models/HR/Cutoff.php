<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class Cutoff extends Model
{
    protected $fillable = ['name', 'frequency', 'pay_1', 'pay_2', 'pay_4'];
}
