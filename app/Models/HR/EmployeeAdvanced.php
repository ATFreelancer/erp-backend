<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeAdvanced extends Model
{
    protected $fillable = ['employee_id', 'type', 'effect_date', 'pay_date', 'total' ,'data'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }

    public function item() {
        return $this->hasMany('App\Models\HR\EmployeeAdvancedDetail')->where('inventory_id', '!=', null);
    }

    public function cash() {
        return $this->hasMany('App\Models\HR\EmployeeAdvancedDetail')->where('inventory_id', null);
    }

    public function details() {
        return $this->hasMany('App\Models\HR\EmployeeAdvancedDetail');
    }
}
