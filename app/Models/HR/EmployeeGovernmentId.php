<?php

namespace App\Models\HR;

use Illuminate\Database\Eloquent\Model;

class EmployeeGovernmentId extends Model
{
    protected $fillable = ['employee_id','government_id_type_id','number','expiration_date'];

    public function employee() {
        return $this->belongsTo('App\Models\HR\Employee');
    }

    public function governmentId() {
        return $this->belongsTo('App\Models\HR\EmployeeGovernmentId');
    }
}
