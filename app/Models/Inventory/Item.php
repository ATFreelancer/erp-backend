<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name','company_id','unit_id','unit_value','packing_id','packing_value','unit_price','net_price','retail_price','wholesale_per_box','wholesale_per_piece','item_category_id','description'];
    
    public function company() {
    	return $this->belongsTo('App\Models\Commons\Company');
    }

    public function itemCategory() {
    	return $this->belongsTo('App\Models\Inventory\itemCategory');
    }

    public function packing() {
    	return $this->belongsTo('App\Models\Commons\Packing');
    }

    public function unit() {
    	return $this->belongsTo('App\Models\Commons\Unit');
    }
}
