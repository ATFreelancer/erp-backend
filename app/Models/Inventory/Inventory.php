<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = ['item_id','quantity','affiliation_id'];

    public function item()
    {
    	return $this->belongsTo('App\Models\Inventory\Item');
    }

    public function affiliation()
    {
    	return $this->belongsTo('App\Models\Commons\Affiliation');
    }

    public function scopeWithRelations($query, $params = [])
    {    	
    	return $query->with($params);
    }
}
