<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class ItemTransfer extends Model
{
    protected $fillable = ['inventory_id','affiliation_sender_id','affiliation_receiver_id', 'quantity'];

    public function inventory()
    {
    	return $this->belongsTo('App\Models\Inventory\Inventory');
    }

    public function sender()
    {
    	return $this->belongsTo('App\Models\Commons\Affiliation', 'affiliation_sender_id');
    }

    public function receiver()
    {
    	return $this->belongsTo('App\Models\Commons\Affiliation', 'affiliation_receiver_id');
    }

    public function affiliation()
    {
    	return $this->belongsTo('App\Models\Commons\Affiliation');
    }
}
