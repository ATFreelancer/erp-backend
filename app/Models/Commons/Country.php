<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name','abbreviation','nationality'];
}
