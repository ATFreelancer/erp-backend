<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $fillable = ['name','description'];
}
