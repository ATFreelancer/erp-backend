<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name','address','email_address','contact_number','description'];
}
