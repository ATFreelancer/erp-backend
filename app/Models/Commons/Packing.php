<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Packing extends Model
{
    protected $fillable = ['name','abbreviation','description'];
}
