<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $fillable = ['name','description'];
}
