<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class GovernmentId extends Model
{
    protected $fillable = ['name','country_id','is_primary'];

    public function country() {
        return $this->belongsTo('App\Models\Commons\Country');
    }
}
