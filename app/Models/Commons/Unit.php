<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = ['name','abbreviation','description'];
}
