<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $fillable = ['name','abbreviation','every_mon','every_tue','every_wed','every_thu','every_fri','every_sat','every_sun','start_date','end_date'];
}
