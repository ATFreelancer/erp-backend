<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class ContactPerson extends Model
{
    protected $fillable = ['company_id','first_name','middle_name','last_name','address','email_address','contact_number'];
    
    public function company() {
    	return $this->belongsTo('App\Models\Commons\Company');
    }
}
