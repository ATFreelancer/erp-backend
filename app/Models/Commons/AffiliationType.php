<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class AffiliationType extends Model
{
    protected $fillable = ['name','description'];
}
