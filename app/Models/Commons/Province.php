<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = ['country_id','name'];

    public function country() {
        return $this->belongsTo('App\Models\Commons\Country');
    }
}
