<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class Affiliation extends Model
{
    protected $fillable = ['name','affiliation_type_id','address','contact_number','email_address','description'];

    public function affiliationType() { 
    	return $this->belongsTo('App\Models\Commons\AffiliationType');
    }
}
