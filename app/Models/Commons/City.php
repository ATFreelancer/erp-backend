<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['province_id','name'];

    public function province() {
        return $this->belongsTo('App\Models\Commons\Province');
    }
}
