<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Repositories\Check;


class Fingerprint extends Model
{
    protected $fillable = ['user_id', 'notes'];
    
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function address()
    {
    	return Check::address();
    }

    public function getRepoAttribute()
    {
    	$ip = Check::address();

    	return 	"http://".$ip."/erp-backend/public/";
    }

    public function getBaseAttribute()
    {
    	$ip = Check::address();

    	return 	"http://".$ip."/erp-backend/app/repositories/";
    }
}
